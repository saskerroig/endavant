#include "Core/CCoreEngine.h"
#include <iostream>
#include <array>
#include <chrono>
#include "Core/CBasicTypes.h"
#include <net/buffer.h>
#include <net/serializable.h>
#include <net/tcp/server.h>
#include <net/tcp/client.h>

using namespace edv;
using namespace net;

struct message : public net::serializable
{
	u8		id;

	message() : id(0) {}

	s32	serialized_size() const
	{
		return sizeof(*this);
	}

	void serialize(buffer<>* dst) const
	{
		dst->write(id);
	}

	void deserialize(buffer<>& orig)
	{
		orig.read(&id);
	}
};


// ejemplo de funcion que procesa un mensaje cli->srv
// el servidor recibe la peticion en "in" y escribe su respuesta en "out"
// param es un parametro opcional que puede usar esta funcion para lo que necesite
void process_message(buffer<>& in, buffer<>& out, void* param)
{
	message msg;
	msg.deserialize(in);

	LOG(LOG_DEVEL, LOGSUB_NETWORK, "servidor recibe mensaje con id: %d", msg.id);

	msg.id = 2;
	msg.serialize(&out);

//	u32 data0 = 0;
//	u8 	data1 = 0;
//
//	in.read_n(&data0, &data1);
//
//	LOG(LOG_DEVEL, LOGSUB_NETWORK, "servidor recibe: %d, %c", data0, data1);
//
//	data0 = 6;
//	data1 = 'b';
//
//	out.write_n(data0, data1);
}

int main(int argc, char *argv[])
{
//	{
//		auto& l_core	= CCoreEngine::Instance();
//		l_core.StartUp();
//
//		const socket::port_t port = 10521;
//		tcp::server srv(port, net::run_info(process_message,NULL));
//		tcp::client cli("localhost", port);
//
//		message msg;
//		msg.id = 1;
//
//		buffer<> buff;
//		msg.serialize(&buff);
//		cli.send(buff);
//
//		buff.reset();
//		tcp::io::recv_status status;
//		do
//		{
//			LOG(LOG_DEVEL, LOGSUB_NETWORK, "esperando mensaje...");
//			status = cli.receive_nb(&buff);
//			std::this_thread::sleep_for(std::chrono::milliseconds(10));
//		}
//		while(!status.data_read);
//		msg.deserialize(buff);
//
//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "cliente recibe respuesta con id: %d", msg.id);

//		u32 data0 = 5;
//		u8	data1 = 'a';
//		const size_t capacity = sizeof(data0)+ sizeof(data1);
//
//		buffer<capacity> buff; // estatico
//		buff.write_n(data0, data1);
//		cli.send(buff);
//
//		data0 = 0;
//		data1 = 'a';
//		buffer<> srv_msg;
//		cli.receive(&srv_msg);
//		srv_msg.read(&data0);
//		srv_msg.read(&data1);
//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "Cliente recibe %d %c", data0, data1);
//
//		std::string t1("multiple"), t2("string"), t3("test");
//		buffer<> test154;
//		test154.write_n(t1, t2, t3);
//
//		std::string r1, r2, r3;
//		test154.reset();
//		test154.read_n(&r1, &r2, &r3);
//
//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "leido: %s %s %s", t1.c_str(), t2.c_str(), t3.c_str());
//
//		t1 = "mixed";
//		u32 v1 = 0x0BADF00D;
//		t2 = "types";
//		buffer<> test182(t1, v1, t2);
//
//		t1 = "";
//		v1 = 0;
//		t2 = "";
//		test182.reset();
//		test182.read_n(&t1, &v1, &t2);
//
//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "leido: %s %x %s", t1.c_str(), v1, t2.c_str());

//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "sleeping...");
//		std::this_thread::sleep_for(std::chrono::seconds(1000));
//		LOG(LOG_DEVEL, LOGSUB_NETWORK, "done.");
//	}

//

//	CCoreEngine &l_core= CCoreEngine::Instance();
//	l_core.StartUp();
//
//	auto	&l_input = CCoreEngine::Instance().GetInputManager();
//	//auto	&l_log = CCoreEngine::Instance().GetLogManager();
//	auto	&l_render = CCoreEngine::Instance().GetRenderManager();
//	auto	&l_timer = CCoreEngine::Instance().GetTimerManager();
//
//
//
//	//Escena principal
//    CScene MiEscena;
//    MiEscena.SetPosition(glm::vec2(0,0));
//	l_render.PushScene(&MiEscena);
//
//
//	//Jugadors
//	l_input.GetKeyboard().InsertKeyAction("P1Up", "Q");
//	l_input.GetKeyboard().InsertKeyAction("P1Down", "A");
//	PongPlayer	l_jugador1(glm::vec2(100, l_render.GetWindowSize().y / 2), "P1Up", "P1Down", s_PathToTextures + "pala1.bmp");
//
//	l_input.GetKeyboard().InsertKeyAction("P2Up", "O");
//	l_input.GetKeyboard().InsertKeyAction("P2Down", "L");
//	PongPlayer	l_jugador2(glm::vec2(l_render.GetWindowSize().x - 100, l_render.GetWindowSize().y / 2),"P2Up", "P2Down", s_PathToTextures + "pala2.bmp");
///*
//	CTriangle	testtriangle(glm::vec2(100,0), glm::vec2(50,50),glm::vec2(0,0),12,glm::vec3(1,0,0) );
//	CCoreEngine::Instance().GetRenderManager().GetCurrentScene()->AddChild(&testtriangle);
//
//	CSprite		testsprite;
//	CCoreEngine::Instance().GetRenderManager().GetCurrentScene()->AddChild(&testsprite);
//	testsprite.InitSprite("src/Tests/Test1/Textures/Exemple.bmp");
//	testsprite.SetPosition(glm::vec2(300,300));
//	testsprite.SetSpriteSize(glm::uvec2(250,250));
//*/
//	CTextTTF	TestText;
//	CCoreEngine::Instance().GetRenderManager().GetCurrentScene()->AddChild(&TestText);
//	TestText.InitTextTTF(s_PathToFonts + "arial.ttf");
//	TestText.SetTextTTF("PUTO EDU!! LES PUTES FONTS FUNCIONEN MADARFACKARRRRRR", EV_Color{0,128,128,0});
//	TestText.SetPosition(glm::vec2(400,300));
//	TestText.SetRotation(45.0);
//
//
//
//
//
//
//	while ( l_core.IsRunning() )
//	{
//		// Actualitzo l'engine
//		l_core.Update();
//
//
//		// Actualitzo els jugadors
//		auto dt = l_timer.GetElapsedTimeSeconds();
//		l_jugador1.Update(dt);
//		l_jugador2.Update(dt);
//
//		// Pinto
//		l_core.Render();
//	}
//
//	l_core.ShutDown();

	return 0;
}

