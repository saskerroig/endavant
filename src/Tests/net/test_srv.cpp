#include <iostream>
#include "Core/CCoreEngine.h"
#include "net/tcp/server.h"
#include "net/buffer.h"
#include "net/socket.h"
#include "net/msg_handler.h"

using namespace edv::net;

class handler_example : public msg_handler
{
public:
	handler_example(std::string&& message_out) : message_out_(message_out) { }

	void process_message(buffer<>& in, buffer<>* out) override
	{
		std::string message_in;
		in.read(&message_in);

		LOG(LOG_INFO, LOGSUB_GAME, "server receives: %s", message_in.c_str());

		out->write(message_out_);
	}

private:
	std::string message_out_;
};

int main(int argc, char* argv[])
{
	CCoreEngine& core = CCoreEngine::Instance();
	core.StartUp();

	const socket::port_t port = 7777;
	std::unique_ptr<msg_handler> handler(new handler_example("server processed your petition"));

	try
	{
		tcp::server srv(port, handler.get());
		LOG(LOG_INFO, LOGSUB_GAME, "test server started");

		core.Run();
	}
	catch(const std::exception& e)
	{
		LOG(LOG_INFO, LOGSUB_GAME, e.what());
	}
	catch(...)
	{
		LOG(LOG_INFO, LOGSUB_GAME, "unknown exception thrown");
	}

	LOG(LOG_INFO, LOGSUB_GAME, "test server exiting");
	core.ShutDown();

	return 0;
}
