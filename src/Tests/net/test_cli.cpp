#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "Core/CCoreEngine.h"
#include "net/tcp/client.h"
#include "net/buffer.h"
#include "net/socket.h"

using namespace edv::net;

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "usage:" << std::endl;
		std::cout << "\t" << argv[0] << " ip/host message" << std::endl;
		exit(1);
	}

	std::string host(argv[1]);
	std::string message;

	for (s32 argument = 2; argument < argc; argument++)
		message += std::string(argv[argument]) + " ";

	CCoreEngine::Instance().StartUp();

	tcp::client cli(host, 7777);

	buffer<> buff(message);
	cli.send(buff);

	cli.receive(&buff);
	buff.read(&message);

	LOG(LOG_INFO, LOGSUB_NETWORK, "client rep resposta del server: %s", message.c_str());
	LOG(LOG_INFO, LOGSUB_NETWORK, "surt el client");

	return 0;
}
