#include "Utils/math.h"

namespace edv	{
namespace math	{

s32 div_add_remainder(s32 dividend, s32 divisor)
{
	s32 result = dividend / divisor;

	if (dividend % divisor)
		result++;

	return result;
}

}
}
