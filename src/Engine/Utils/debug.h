#ifndef EDV_DEBUG_DEBUG_H_
#define EDV_DEBUG_DEBUG_H_

#include <string>
#include "Core/CBasicTypes.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Core/Defines.h"

#ifdef EDV_LINUX
#include <execinfo.h>
#endif

#define MAX_FRAMES	20

namespace edv	{
namespace debug	{

//@TODO: demangle
//@TODO: windows
std::string	backtrace(u32 from_frame = 0);
void 		print_backtrace(u32 from_frame = 0);

}
}

#endif
