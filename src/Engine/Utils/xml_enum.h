#ifndef EDV_UTILS_STRENUM_H_
#define EDV_UTILS_STRENUM_H_

#include <cstddef>
#include <cassert>
#include <array>
#include "Utils/CConversions.h"
#include "Utils/CXMLParserPUGI.h"

namespace edv	{
namespace util	{

template <typename T, size_t Size>
class xml_enum
{
public:
	typedef	std::array<std::string, Size>	container_t;

	xml_enum(const std::string& file, const std::string& path, const std::string& attribute = "name");
	container_t	strings() const;
	std::string operator[](const T& value) const;

private:
	container_t	strings_;
};

template <typename T, size_t Size>
xml_enum<T,Size>::xml_enum(const std::string& file, const std::string& path, const std::string& attribute)
{
	CXMLParserPUGI Parser(file);
	u32 size = Parser.GetNodeNameCount(path);
	if (size)
	{
		assert(size + 1/*NONE*/ == Size);	// correspondencia tamanyo enum / xml
		strings_[0] = "NONE";
		for (u32 idx = 1; idx < Size; idx++)
		{
			const std::string node(path + "#" + to_string(idx-1));
			strings_[idx] = Parser.GetStringAttributeValue(node, attribute, "");
			assert(!strings_[idx].empty());		// falta atributo name
		}
	}

	assert(size); // no se encuentra el tag
}

template <typename T, size_t Size>
typename xml_enum<T,Size>::container_t xml_enum<T,Size>::strings() const
{
	return strings_;
}

template <typename T, size_t Size>
std::string xml_enum<T,Size>::operator[](const T& value) const
{
	for (size_t idx = 0; idx < Size; idx++)
	{
		if (static_cast<T>(idx) == value)
			return strings_[idx];
	}

	return std::string();
}

}
}

#endif
