#include "debug.h"

namespace edv	{
namespace debug	{

std::string backtrace(u32 from_frame)
{
	std::string trace;
#ifdef EDV_LINUX
	void* trace_arr[MAX_FRAMES];
	s32 trace_size = ::backtrace(trace_arr, MAX_FRAMES);
	char** sym = backtrace_symbols(trace_arr, trace_size);
	for (s32 idx = static_cast<s32>(from_frame); idx < trace_size; idx++)
		trace += std::string(sym[idx]) + "\n";
	free(sym);
#endif
	return trace;
}

void print_backtrace(u32 from_frame)
{
#ifdef EDV_LINUX
	LOG(LOG_INFO, LOGSUB_ENGINE, "\n%s", backtrace(from_frame).c_str());
#endif
}


}
}

