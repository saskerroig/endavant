#ifndef EDV_CALLBACK_H_
#define EDV_CALLBACK_H_

namespace edv	{
namespace util	{

class callback
{
public:
	typedef void (*func_t)(void*,void*);

	callback() : object_(nullptr), function_(nullptr), called_(false) { }
	callback(void* object, func_t function) : object_(object), function_(function), called_(false) { }
	virtual ~callback() { }

	void	call(void* caller)	{ if (!empty()) {function_(object_, caller); called_ = true;} }
	bool	empty()				{ return object_ == nullptr;	}
	bool	called()			{ return called_;				}
	void	reset()				{ called_ = false;				}

private:
	void*	object_;
	func_t	function_;
	bool	called_;
};

}
}

#endif
