#ifndef EDV_MATH_MATH_H_
#define EDV_MATH_MATH_H_

#include "Core/CBasicTypes.h"

namespace edv	{
namespace math	{

s32 div_add_remainder(s32 dividend, s32 divisor);

}
}

#endif
