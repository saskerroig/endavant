#ifndef CLOGMANAGER_H_
#define CLOGMANAGER_H_

#include <vector>
#include <iostream>
#include <cstdarg>
#include <string>
#include <fstream>
#include <thread>
#include <mutex>
#include "Defines.h"
#include "Utils/xml_enum.h"

enum TLogLevel
{
	LOG_NONE = 0,
	LOG_INFO,
	LOG_WARNING,
	LOG_ERROR,
	LOG_DEVEL,	//Tipos de info, warning o error que se necesiten para desarrollar, luego no apareceran en version Release.

	LOG_ALL
};

enum TLogSubsystem
{
	LOGSUB_NONE = 0,
	LOGSUB_VIDEO,
	LOGSUB_INPUT,
	LOGSUB_PARSER,
	LOGSUB_GUI,
	LOGSUB_GAME,
	LOGSUB_SOUND,
	LOGSUB_ENGINE,
	LOGSUB_EVENTS,
	LOGSUB_TIMER,
	LOGSUB_RESOURCES,
	LOGSUB_NETWORK,
	LOGSUB_STATE,
	LOGSUB_ALL
};


#define LOG(level, subsystem, text, args...)\
	CCoreEngine::Instance().GetLogManager().LogOutput(level, subsystem, std::this_thread::get_id(), text, ##args)

class CLogManager
{
public:

	void 	LogOutput(TLogLevel, TLogSubsystem, std::thread::id&& thread_id, std::string, ...);

	void	StartUp();
	void	ShutDown();

	bool	m_IsLogEnabled;

	~CLogManager();
	CLogManager();	friend class CoreEngine;
private:
	struct CLogType
	{
		std::ofstream	*m_logfile;
		bool			m_IsConsole;
		TLogLevel		m_loglevel;
		TLogSubsystem	m_logsubsystem;

		CLogType():m_logfile(NULL),m_IsConsole(true),m_loglevel(LOG_NONE),m_logsubsystem(LOGSUB_NONE) {};
	};

	std::vector< CLogType >							m_Loggers;
	edv::util::xml_enum<TLogLevel, LOG_ALL>			loglevels_;
	edv::util::xml_enum<TLogSubsystem, LOGSUB_ALL>	logsubsystems_;
	std::mutex										write_mutex_;
	
	inline	void	WriteOutputLog(TLogLevel, TLogSubsystem, std::string);

	TLogLevel		ParseLogLevelOptions( std::string);
	TLogSubsystem	ParseLogSubSystemOptions( std::string);
};

#endif /* CLOGMANAGER_H_ */

