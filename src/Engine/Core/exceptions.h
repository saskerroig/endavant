#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <exception>
#include <string>

namespace edv {
namespace core {
namespace exc {

class fatal_error: public std::runtime_error
{
	public:
		explicit fatal_error(const std::string &s):
			std::runtime_error(s){};
};



}
}
}

#endif /* EXCEPTIONS_H_ */
