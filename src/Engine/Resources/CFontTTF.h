/*
 * CFontTTF.h
 *
 *  Created on: 02/07/2013
 *      Author: Dani
 */

#ifndef CFONTTTF_H_
#define CFONTTTF_H_

#include <Core/CBasicTypes.h>
#include <Renderer/CRenderTypes.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class CFontTTF
{
public:
	CFontTTF();
	CFontTTF(const std::string &aFontName, const u32 aFontSize, TTF_Font *aFontPtr);

	SDL_Surface* GetTextSurface(const std::string &aText, const EV_Color &aColor);

	~CFontTTF();
public:
	std::string m_ID;
	std::string	m_FontName;
	u32			m_FontSize;
	TTF_Font 	*m_FontPtr;
	s32 		m_RefCount;
};
#endif /* CFONTTTF_H_ */
