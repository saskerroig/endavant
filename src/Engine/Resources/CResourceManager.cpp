#include <sstream>
#include "CResourceManager.h"
#include "Utils/CXMLParserPUGI.h"
#include "Core/Defines.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


CResourceManager::CResourceManager()
{

}

CResourceManager::~CResourceManager()
{

}

void CResourceManager::StartUp(void)
{
	//SDL_Image
	if ( !IMG_Init(IMG_INIT_PNG) )
	{
		LOG( LOG_ERROR, LOGSUB_RESOURCES,"Starting Up! (FAILED LOADING SDL_Image)");
		return;
	}

	LOG( LOG_INFO, LOGSUB_RESOURCES,"Starting Up! SDL_Image Loaded!");

//	LoadTextures();
	LoadAudio();
}

void CResourceManager::ShutDown(void)
{
	LOG( LOG_INFO, LOGSUB_RESOURCES,"ShutDown!");
	InternalReleaseResources();
	//	FreeTextures();
	FreeAudio();
}

void CResourceManager::Update(f64 dt)
{

}

CGLTexture* CResourceManager::LoadTexture(const std::string& a_ID, const std::string& a_PathToTexture)
{
	t_Textures::iterator itTexture = m_Textures.find(a_ID);
	if (itTexture != m_Textures.end())
	{
		itTexture->second->m_RefCount++;
		return itTexture->second;
	}

	itTexture = m_Textures.begin();
	while (itTexture != m_Textures.end())
	{
		if (itTexture->second->m_TextureName == a_PathToTexture)
		{
			itTexture->second->m_RefCount++;
			return itTexture->second;
		}
		itTexture++;
	}

	CGLTexture *Texture = new CGLTexture;
	if ( Texture->LoadTextureFromFile(a_PathToTexture) )
	{
		m_Textures[a_ID] = Texture;
		return Texture;
	}
	delete Texture;

	return nullptr;
}

CGLTexture* CResourceManager::LoadTexture(const std::string& a_PathToTexture)
{
	return LoadTexture(a_PathToTexture, a_PathToTexture);
}

bool CResourceManager::ReleaseTexture(const std::string& a_ID_PathToTexture)
{
	bool retval = false;
	t_Textures::iterator itTexture = m_Textures.find(a_ID_PathToTexture);
	if (itTexture != m_Textures.end())
	{
		if ( InternalReleaseTexture( itTexture->second ) )
		{
			m_Textures.erase(itTexture++);
			retval = true;
		}
	}
	else
	{
		itTexture = m_Textures.begin();
		while (itTexture != m_Textures.end())
		{
			if (itTexture->second->m_TextureName == a_ID_PathToTexture)
			{
				if ( InternalReleaseTexture( itTexture->second ) )
				{
					t_Textures::iterator itTextureCopy = itTexture;
					itTexture++;
					m_Textures.erase(itTextureCopy);
					retval = true;
					continue;
				}
			}
			itTexture++;
		}
	}
	return retval;
}


bool CResourceManager::ReleaseTexture(CGLTexture* aTexture)
{
	bool retval = false;
	t_Textures::iterator itTexture = m_Textures.begin();
	while (itTexture != m_Textures.end())
	{
		if (itTexture->second == aTexture)
		{
			if ( InternalReleaseTexture( itTexture->second ) )
			{
				t_Textures::iterator itTextureCopy = itTexture;
				itTexture++;
				m_Textures.erase(itTextureCopy);
				retval = true;
				continue;
			}
		}
		itTexture++;
	}
	return retval;
}

bool CResourceManager::InternalReleaseTexture(CGLTexture* aResourceTexture, bool aForce)
{
	aResourceTexture->m_RefCount--;
	if ( (aResourceTexture->m_RefCount <= 0) || aForce )
	{
		LOG( LOG_INFO, LOGSUB_RESOURCES, "Delete Texture: %s", aResourceTexture->m_TextureName.c_str());
		delete (aResourceTexture);
		return true;
	}
	return false;
}

void CResourceManager::ParseXMLAudioGroup(CXMLParserPUGI& a_Parser, const std::string& a_NodePath, std::vector< std::string >& a_IDs, std::vector< std::string >& a_Paths)
{
	/*
	 * TODO OOOOOOOOOOOOOOOOOOOOOOOOOO
	 */
	/*a_IDs.clear();
	a_Paths.clear();

	uint Elements = a_Parser.GetNodeNameCount(a_NodePath);
	for (uint Element = 0; Element < Elements; Element++)
	{
		std::string NodePath(a_NodePath);
		NodePath.append("#");
		std::stringstream ss;
		ss << Element;
		NodePath.append(ss.str());

		a_IDs.push_back(a_Parser.GetStringAttributeValue(NodePath,"Id"));
		std::string Path(PATH_AUDIO);
		Path.append(a_Parser.GetStringAttributeValue(NodePath,"Path"));
		a_Paths.push_back(Path);
	}
	*/
}

bool CResourceManager::LoadAudio()
{
	//TODO
	/*
	CXMLParserPUGI Parser(EVTENGINE::CONFIG::AUDIO_CONFIG_XML_PATH);

	std::vector< std::string > IDs, Paths;

	ParseXMLAudioGroup(Parser, "AUDIO/SOUNDS/SOUND", IDs, Paths);
	for (uint index = 0; index < IDs.size(); index++)
	{
		m_Sounds[IDs[index]] = new CSoundEffect(IDs[index], Paths[index]);
	}

	ParseXMLAudioGroup(Parser, "AUDIO/TRACKS/MUSIC", IDs, Paths);
	for (uint index = 0; index < IDs.size(); index++)
	{
		m_Music[IDs[index]] = new CMusicTrack(IDs[index], Paths[index]);
	}
	 */
	return false;
}

CFontTTF* CResourceManager::GetFont(const std::string& a_ID)
{
	t_Fonts::iterator itFont = m_Fonts.find(a_ID);
	if (itFont != m_Fonts.end())
	{
		itFont->second->m_RefCount++;
		return itFont->second;
	}
	return nullptr;
}

CFontTTF* CResourceManager::GetFont(const std::string& aName, const u32& aSize)
{
	t_Fonts::iterator itFont = m_Fonts.begin();
	while (itFont != m_Fonts.end())
	{
		if ( (itFont->second->m_FontSize == aSize) && (itFont->second->m_FontName == aName) )
		{
			itFont->second->m_RefCount++;
			return itFont->second;
		}
		itFont++;
	}
	return nullptr;
}

CFontTTF* CResourceManager::LoadFont(const std::string& aName, const u32& aSize)
{
	std::string FontID("");
	FontID = aName + "#" + to_string(aSize);
	return LoadFont( FontID, aName, aSize);
}

CFontTTF* CResourceManager::LoadFont(const std::string& a_ID, const std::string& aName, const u32& aSize)
{
	CFontTTF *fontptr = nullptr;

	fontptr = GetFont( a_ID);
	if (fontptr) return fontptr;

	fontptr = GetFont( aName, aSize);
	if (fontptr) return fontptr;

	fontptr = new CFontTTF(aName, aSize, nullptr);
	if (InternalLoadFont( fontptr ))
	{
		m_Fonts[a_ID] = fontptr;
		return m_Fonts[a_ID];
	}
	delete fontptr;
	return nullptr;
}

bool CResourceManager::ReleaseFont(const std::string& a_ID)
{
	bool retval = false;
	t_Fonts::iterator itFont = m_Fonts.find(a_ID);
	if (itFont != m_Fonts.end())
	{
		if ( InternalReleaseFont( itFont->second ) )
		{
			m_Fonts.erase(itFont++);
			retval = true;
		}
	}
	return retval;
}

bool CResourceManager::ReleaseFont(const std::string& aName, const u32& aSize)
{
	bool retval = false;
	t_Fonts::iterator itFont = m_Fonts.begin();
	while (itFont != m_Fonts.end())
	{
		if (itFont->second->m_FontSize == aSize && itFont->second->m_FontName == aName)
		{
			if ( InternalReleaseFont( itFont->second ) )
			{
				t_Fonts::iterator itFontCopy = itFont;
				itFont++;
				m_Fonts.erase(itFontCopy);
				retval = true;
				continue;
			}
		}
		itFont++;
	}
	return retval;
}

bool CResourceManager::InternalLoadFont(CFontTTF *aResourceFont)
{
	aResourceFont->m_FontPtr	= TTF_OpenFont(aResourceFont->m_FontName.c_str(), aResourceFont->m_FontSize);
	if (aResourceFont->m_FontPtr == nullptr)
	{
		LOG( LOG_ERROR, LOGSUB_RESOURCES, "Unable to load font: %s %s \n", aResourceFont->m_FontName.c_str(), TTF_GetError() );
		return false;
	}
	return true;
}

bool CResourceManager::ReleaseFont(CFontTTF* aFont)
{
	bool retval = false;
	t_Fonts::iterator itFont = m_Fonts.begin();
	while (itFont != m_Fonts.end())
	{
		if (itFont->second == aFont)
		{
			if ( InternalReleaseFont( itFont->second ) )
			{
				t_Fonts::iterator itFontCopy = itFont;
				itFont++;
				m_Fonts.erase(itFontCopy);
				retval = true;
				continue;
			}
		}
		itFont++;
	}
	return retval;
}

bool CResourceManager::InternalReleaseFont(CFontTTF *aResourceFont, bool aForce)
{
	aResourceFont->m_RefCount--;
	if ( (aResourceFont->m_RefCount <= 0) || aForce )
	{
		if (aResourceFont->m_FontPtr != nullptr)
		{
			TTF_CloseFont(aResourceFont->m_FontPtr);
			LOG( LOG_INFO, LOGSUB_RESOURCES, "Delete Font: %s", aResourceFont->m_FontName.c_str() );
			delete (aResourceFont);
			return true;
		}
	}
	return false;
}

void CResourceManager::LogFontResources()
{
	t_Fonts::iterator itFont = m_Fonts.begin();
	while (itFont != m_Fonts.end())
	{
		LOG( LOG_INFO, LOGSUB_RESOURCES, "FONT: ID:%s Size:%u Path:%s RefCount:%d", itFont->first.c_str(), itFont->second->m_FontSize, itFont->second->m_FontName.c_str(), itFont->second->m_RefCount );
		itFont++;
	}
}

void CResourceManager::LogTextureResources()
{
	t_Textures::iterator itTexture = m_Textures.begin();
	while (itTexture != m_Textures.end())
	{
		LOG( LOG_INFO, LOGSUB_RESOURCES, "TEXTURE: ID:%s RefCount:%d", itTexture->first.c_str(), itTexture->second->m_RefCount );
		itTexture++;
	}
}

void CResourceManager::LogResources()
{
	LogFontResources();
	LogTextureResources();
}

void CResourceManager::InternalReleaseResources()
{
	t_Textures::iterator itTexture = m_Textures.begin();
	while (itTexture != m_Textures.end())
	{
		InternalReleaseTexture( itTexture->second, true );
		m_Textures.erase(itTexture++);
	}
	t_Fonts::iterator itFont = m_Fonts.begin();
	while (itFont != m_Fonts.end())
	{
		InternalReleaseFont( itFont->second, true );
		m_Fonts.erase(itFont++);
	}
}

void CResourceManager::FreeAudio()
{
	/*
	for (t_Sounds::iterator itSnd = m_Sounds.begin(); itSnd != m_Sounds.end(); itSnd++)
	{
		delete itSnd->second; // liberamos si o si
	}
	m_Sounds.clear();

	for (t_Music::iterator itMus = m_Music.begin(); itMus != m_Music.end(); itMus++)
	{
		delete itMus->second;
	}
	m_Music.clear();
	*/
}

CSoundEffect* CResourceManager::GrabSound(const std::string& a_ID)
{/*
	t_Sounds::iterator itSnd = m_Sounds.find(a_ID);
	if (itSnd != m_Sounds.end())
	{
		CSoundEffect* Sound = itSnd->second;

		if (!Sound->IsLoaded())
			Sound->Load();

		Sound->Grab();
		return Sound;
	}
*/
	return NULL;
}

void CResourceManager::DropSound(const std::string& a_ID)
{
	/*
	t_Sounds::iterator itSnd = m_Sounds.find(a_ID);
	if (itSnd != m_Sounds.end())
	{
		CSoundEffect* Sound = itSnd->second;

		if (Sound->GetCount() > 1)
			Sound->Drop();
		else
			Sound->UnLoad();
	}
	*/
}

void CResourceManager::DropSound(const std::string& a_ID, u32 a_Count)
{
	/*
	t_Sounds::iterator itSnd = m_Sounds.find(a_ID);
	if (itSnd != m_Sounds.end())
	{
		CSoundEffect* Sound = itSnd->second;

		u32 cnt = a_Count;
		while(cnt)
		{
			if (Sound->GetCount() > 1)
				Sound->Drop();
			else
			{
				Sound->UnLoad();
				break;
			}

			cnt--;
		}
	}
	*/
}

CMusicTrack* CResourceManager::GetTrack(const std::string& a_ID)
{
	/*
	CMusicTrack* Track = NULL;

	t_Music::iterator itMus = m_Music.find(a_ID);
	if (itMus != m_Music.end())
	{
		Track = itMus->second;
		if (!Track->IsLoaded())
			Track->Load();
	}

	return Track;
	*/
	return nullptr;
}
