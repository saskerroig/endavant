#ifndef CRESOURCEMANAGER_H_
#define CRESOURCEMANAGER_H_

#include "Core/ISubSystem.h"
#include "Core/CBasicTypes.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <map>
#include <string>
#include <vector>
#include "Renderer/GL/CGLTexture.h"
#include "Resources/CFontTTF.h"

class CSoundEffect;
class CMusicTrack;
class CXMLParserPUGI;

class CResourceManager: public ISubSystem
{
public:
	typedef std::map< std::string, CFontTTF* >	t_Fonts;
	typedef std::map< std::string, CGLTexture* >	t_Textures;
	typedef std::map< std::string, CSoundEffect* >	t_Sounds;
	typedef std::map< std::string, CMusicTrack* >	t_Music;

	CResourceManager();
	virtual ~CResourceManager();

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);

	void LogResources();

	//Finds and returns the font
	CFontTTF* GetFont(const std::string& a_ID);
	CFontTTF* GetFont(const std::string& aName, const u32 &aSize);
	CFontTTF* LoadFont(const std::string& aName, const u32 &aSize);
	CFontTTF* LoadFont(const std::string& a_ID, const std::string& aName, const u32 &aSize);
	bool ReleaseFont(const std::string& a_ID);
	bool ReleaseFont(const std::string& aName, const u32 &aSize);
	bool ReleaseFont(CFontTTF* aFont);
	void LogFontResources();

	CGLTexture* LoadTexture(const std::string& a_ID, const std::string& a_PathToTexture);
	CGLTexture* LoadTexture(const std::string& a_PathToTexture);
	bool ReleaseTexture(const std::string& a_ID_PathToTexture);
	bool ReleaseTexture(CGLTexture* aTexture);
	void LogTextureResources();

	CSoundEffect* GrabSound(const std::string& a_ID);
	void DropSound(const std::string& a_ID);
	void DropSound(const std::string& a_ID, u32 a_Count);

	CMusicTrack* GetTrack(const std::string& a_ID);

private:
	bool InternalLoadFont(CFontTTF *aResourceFont);
	bool InternalReleaseFont(CFontTTF *aResourceFont, bool aForce = false );
	bool InternalReleaseTexture(CGLTexture *aResourceTexture, bool aForce = false );

	void InternalReleaseResources();

	void ParseXMLAudioGroup(CXMLParserPUGI& a_Parser, const std::string& a_NodePath,
			std::vector< std::string >& a_IDs, std::vector< std::string >& a_Paths);
	bool LoadAudio();
	void FreeAudio();

	t_Fonts			m_Fonts;
	t_Textures		m_Textures;
	t_Sounds		m_Sounds;
	t_Music			m_Music;
};

#endif /* CRESOURCEMANAGER_H_ */
