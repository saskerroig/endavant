/*
 * CResourceLoader.h
 *
 *  Created on: 05/08/2013
 *      Author: SASKERACER
 */

#ifndef CRESOURCELOADER_H_
#define CRESOURCELOADER_H_

#include "Core/CBasicTypes.h"
#include <thread>

class CResourceLoader
{
public:
	CResourceLoader();
	virtual ~CResourceLoader();

	void LoadResourcesASync( const std::string &aPath);
	bool LoadResources( const std::string &aPath, const u32 aSleep = 0);

	void ReleaseResourcesASync( const std::string &aPath);
	bool ReleaseResources( const std::string &aPath, const u32 aSleep = 0);

	bool IsLoaded() { return m_Loaded; }

public:
	std::string m_Path;
private:
	bool		m_Loaded;
	bool		m_LoadInited;
	bool		m_Destroy;
	u32			m_LoadedElements;
	std::thread m_Thread;

	static void UpdateLoader( CResourceLoader *aResourceLoader);
	static void UpdateReleaser( CResourceLoader *aResourceLoader);
};



#endif /* CRESOURCELOADER_H_ */
