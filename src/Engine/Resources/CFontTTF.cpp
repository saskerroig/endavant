
#include "CFontTTF.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Resources/CResourceManager.h"

CFontTTF::CFontTTF()
{
	m_ID = "";
	m_FontName = "";
	m_FontSize = 0;
	m_FontPtr = nullptr;
	m_RefCount = 1;
}

CFontTTF::CFontTTF(const std::string &aFontName, const u32 aFontSize, TTF_Font *aFontPtr)
{
	m_ID = "";
	m_FontName = aFontName;
	m_FontSize = aFontSize;
	m_FontPtr = aFontPtr;
	m_RefCount = 1;
}

CFontTTF::~CFontTTF()
{
}

SDL_Surface* CFontTTF::GetTextSurface(const std::string &Text,const EV_Color &aColor)
{
	SDL_Surface * l_surface = TTF_RenderUTF8_Blended(m_FontPtr, Text.c_str(), aColor);
	//SDL_Surface * l_surface = TTF_RenderUTF8_Solid(m_pTTFont, Text.c_str(), aColor);
	return l_surface;
}



