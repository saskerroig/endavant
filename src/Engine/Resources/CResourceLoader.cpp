/*
 * CResourceLoader.cpp
 *
 *  Created on: 05/08/2013
 *      Author: SASKERACER
 */


#include "CResourceLoader.h"
#include "CResourceManager.h"
#include "Utils/CXMLParserPUGI.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CResourceLoader::CResourceLoader():
m_Path(""),
m_Loaded(false),
m_LoadInited(false),
m_Destroy(false),
m_LoadedElements(0),
m_Thread()
{
}

CResourceLoader::~CResourceLoader()
{
	m_Destroy = true;
	if ( (m_LoadInited) && (!m_Loaded) )
	{
		while (!m_Loaded)
		{
			LOG( LOG_INFO, LOGSUB_RESOURCES,"Waiting thread to destroy object");
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}
	if (m_Thread.joinable())
		m_Thread.join();
	if ( CCoreEngine::Instance().IsRunning())
	{
		ReleaseResources(m_Path);
	}
}

void CResourceLoader::LoadResourcesASync(const std::string& aPath)
{
	m_Loaded = false;
	m_LoadInited = false;
	m_Destroy = false;
	m_LoadedElements = 0;
	m_Path = aPath;
	m_Thread = std::thread(UpdateLoader, this);
}

bool CResourceLoader::LoadResources(const std::string& aPath, const u32 aSleep)
{
	bool retval = false;

	m_LoadInited = true;
	CXMLParserPUGI Parser(aPath);
	if (!Parser.Ready())
	{
		LOG( LOG_ERROR, LOGSUB_RESOURCES,"FAILED LOADING %s", aPath.c_str());
	}
	else
	{
		u32 ResourcesCount = Parser.GetNodeNameCount("RESOURCES/ITEM");
		m_LoadedElements = 0;

		while ( (m_LoadedElements < ResourcesCount) && (!m_Destroy) )
		{
			std::string ResourceNode("RESOURCES/ITEM#");
			ResourceNode += to_string( m_LoadedElements );

			std::string l_Type = Parser.GetStringAttributeValue( ResourceNode, "Type");
			std::string l_ID   = Parser.GetStringAttributeValue( ResourceNode, "ID");
			std::string l_Name = Parser.GetStringAttributeValue( ResourceNode, "Name");
			s32 l_Size = Parser.GetIntAttributeValue( ResourceNode, "Size");

			if ( l_Type == "FONT" )
			{
				CCoreEngine::Instance().GetResourceManager().LoadFont( l_ID, l_Name, l_Size);
			}
			else if ( l_Type == "TEXTURE" )
			{
				CCoreEngine::Instance().GetResourceManager().LoadTexture( l_ID, l_Name);
			}
			else if ( l_Type == "SOUND" )
			{

			}
			else
			{
				LOG( LOG_WARNING, LOGSUB_RESOURCES,"UNKNOWN TAG %s", l_Type.c_str());
			}

			m_LoadedElements++;
			if (aSleep > 0 )
				std::this_thread::sleep_for(std::chrono::milliseconds(aSleep));
		}
		retval = true;
	}
	m_Loaded = true;
	return retval;
}

void CResourceLoader::ReleaseResourcesASync(const std::string& aPath)
{
	m_Path = aPath;
	m_Thread = std::thread(UpdateReleaser, this);
}

bool CResourceLoader::ReleaseResources(const std::string& aPath, const u32 aSleep)
{
	bool retval = false;

	CXMLParserPUGI Parser(aPath);
	if (!Parser.Ready())
	{
		LOG( LOG_ERROR, LOGSUB_RESOURCES,"FAILED RELEASING %s", aPath.c_str());
	}
	else
	{
		u32 ResourcesCount = Parser.GetNodeNameCount("RESOURCES/ITEM");
		m_LoadedElements = 0;

		while ( (m_LoadedElements < ResourcesCount) )
		{
			std::string ResourceNode("RESOURCES/ITEM#");
			ResourceNode += to_string( m_LoadedElements );

			std::string l_Type = Parser.GetStringAttributeValue( ResourceNode, "Type");
			std::string l_ID   = Parser.GetStringAttributeValue( ResourceNode, "ID");
			std::string l_Name = Parser.GetStringAttributeValue( ResourceNode, "Name");
			s32 l_Size = Parser.GetIntAttributeValue( ResourceNode, "Size");

			if ( l_Type == "FONT" )
			{
				if (!CCoreEngine::Instance().GetResourceManager().ReleaseFont( l_ID))
				{
					CCoreEngine::Instance().GetResourceManager().ReleaseFont( l_Name, l_Size);
				}
			}
			else if ( l_Type == "TEXTURE" )
			{
				if (!CCoreEngine::Instance().GetResourceManager().ReleaseTexture( l_ID))
				{
					CCoreEngine::Instance().GetResourceManager().ReleaseTexture( l_Name);
				}
			}
			else if ( l_Type == "SOUND" )
			{

			}
			else
			{
				LOG( LOG_WARNING, LOGSUB_RESOURCES,"UNKNOWN TAG %s", l_Type.c_str());
			}

			m_LoadedElements++;
			if (aSleep > 0 )
				std::this_thread::sleep_for(std::chrono::milliseconds(aSleep));
		}
		retval = true;
	}
	return retval;
}

void CResourceLoader::UpdateLoader(CResourceLoader* aResourceLoader)
{
	LOG( LOG_INFO, LOGSUB_RESOURCES,"THREAD LOADER INIT");
	aResourceLoader->LoadResources( aResourceLoader->m_Path, 50 );
	LOG( LOG_INFO, LOGSUB_RESOURCES,"THREAD LOADER END");
}

void CResourceLoader::UpdateReleaser(CResourceLoader* aResourceLoader)
{
	LOG( LOG_INFO, LOGSUB_RESOURCES,"THREAD RELEASER INIT");
	aResourceLoader->ReleaseResources( aResourceLoader->m_Path, 50 );
	LOG( LOG_INFO, LOGSUB_RESOURCES,"THREAD RELEASER END");
}
