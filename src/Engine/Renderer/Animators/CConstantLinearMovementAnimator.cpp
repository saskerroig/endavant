#include "Renderer/Animators/CConstantLinearMovementAnimator.h"
//#debug
#include "Core/CCoreEngine.h"

CConstantLinearMovementAnimator::CConstantLinearMovementAnimator(glm::vec3 Origin, glm::vec3 Destination, f64 Time, const edv::util::callback& Callback) :
	CCallbackAnimator(Callback),
	m_OffsetPerUT((Destination.x-Origin.x)/Time, (Destination.y-Origin.y)/Time, (Destination.z-Origin.z)/Time),
	m_RemainingTime(Time)
{

}

CConstantLinearMovementAnimator::~CConstantLinearMovementAnimator()
{

}

void CConstantLinearMovementAnimator::Animate(CBaseNode* Node, f64 dt)
{
	if (!Finished())
		Advance(Node, dt);

	CCallbackAnimator::Animate(Node, dt);
}

bool CConstantLinearMovementAnimator::Finished()
{
	return m_RemainingTime <= 0;
}

void CConstantLinearMovementAnimator::Finish(CBaseNode* Node)
{
	Advance(Node, m_RemainingTime);
}

void CConstantLinearMovementAnimator::Advance(CBaseNode* Node, f64 Time)
{
	glm::vec3 OldPosition = Node->GetPosition();

	Node->SetPosition(	glm::vec3(	OldPosition.x + m_OffsetPerUT.x * Time,
									OldPosition.y + m_OffsetPerUT.y * Time,
									OldPosition.z + m_OffsetPerUT.z * Time));

	m_RemainingTime -= Time;
}
