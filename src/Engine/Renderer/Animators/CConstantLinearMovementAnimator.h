#ifndef EDV_ANIMATORS_CCONSTANTLINEARMOVEMENTANIMATOR_H_
#define EDV_ANIMATORS_CCONSTANTLINEARMOVEMENTANIMATOR_H_

#include "Renderer/Animators/CAnimator.h"
#include "Renderer/Base_Nodes/CBaseNode.h"
#include "Renderer/GL/CGLTypes.h"

//#todo iria bien poner las cosas en su namespace para no tener nombres de clase panaderescos
class CConstantLinearMovementAnimator : public CCallbackAnimator
{
public:
	CConstantLinearMovementAnimator(glm::vec3 Origin, glm::vec3 Destination, f64 Time,
			const edv::util::callback& Callback = edv::util::callback());
	~CConstantLinearMovementAnimator();

	void	Animate(CBaseNode* Node, f64 dt) override;
	bool	Finished() override;
	void	Finish(CBaseNode* Node) override;

private:
	void	Advance(CBaseNode* Node, f64 Time);

	glm::vec3 	m_OffsetPerUT;
	f64			m_RemainingTime;
};

#endif
