#ifndef EDV_ANIMATORS_CANIMATOR_H_
#define EDV_ANIMATORS_CANIMATOR_H_

#include "Core/CBasicTypes.h"
#include "Utils/callback.h"

class CBaseNode;

class CAnimator
{
public:
	virtual	~CAnimator() { }

	virtual void	Animate(CBaseNode* Node, f64 dt) = 0;
	virtual bool	Finished() = 0;
	virtual void	Finish(CBaseNode* Node) = 0;
};

class CCallbackAnimator : public CAnimator
{
public:
	CCallbackAnimator(const edv::util::callback& Callback = edv::util::callback()) : m_Callback(Callback) { }
	virtual ~CCallbackAnimator() { }

	void			Animate(CBaseNode* Node, f64 dt) override { if (Finished()) { Notify(); } }
	virtual bool	Finished() = 0;
	virtual void	Finish(CBaseNode* Node) = 0;

private:
	void			Notify() { if (!m_Callback.called()) { m_Callback.call(this); } }

	edv::util::callback	m_Callback;
};

#endif
