#ifndef CSPRITEBATCH_H_
#define CSPRITEBATCH_H_
#include "Renderer/Draw_Nodes/CSpriteBaseBatch.h"


class CSpriteBatch : public CSpriteBaseBatch
{
public:
	CSpriteBatch();
	virtual ~CSpriteBatch();

	virtual void Render();
	virtual void Update(f64 dt);

	void	InitSprite(const std::string &aPathToTexture);


private:

};

#endif /* CSPRITEBATCH_H_ */
