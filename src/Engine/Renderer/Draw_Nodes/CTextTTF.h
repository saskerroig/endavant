
#ifndef CTEXTTTF_H_
#define CTEXTTTF_H_

#include "CSprite.h"
#include "Resources/CFontTTF.h"

class CTextTTF: public CSprite
{
	public:
		CTextTTF();
		void		GetTextTTF(const std::string &aID_TTF);
		void		InitTextTTF(const std::string &aPathToTTF, const u32 &aSize);
		void		SetTextTTF(const std::string& aText, const EV_Color &aColor, const TTextureAlign aAlign = TEXTUREALIGN_LEFTMIDDLE);
		virtual 	~CTextTTF();
	private:
		CFontTTF	*m_TTFont;



};

#endif /* CTEXTTTF_H_ */
