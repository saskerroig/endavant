#include "CSpriteBaseBatch.h"

// TODO: mipmaps?


CSpriteBaseBatch::CSpriteBaseBatch():
CSpriteBase()
{
	m_BatchMode = BATCHMODE_DEPTH;
	m_BatchSize = 1;
	m_BatchPositions.clear();
}

CSpriteBaseBatch::~CSpriteBaseBatch()
{
}

void CSpriteBaseBatch::Render()
{
	CSpriteBase::Render();
}

void CSpriteBaseBatch::Update(f64 dt)
{
	CSpriteBase::Update( dt);
}

void CSpriteBaseBatch::SetBatchSize(const u32 aSize)
{
	if (aSize < 1) return;

	m_BatchSize = aSize;
	m_VBO[0]->LoadBufferData(NULL, m_BatchSize, GL_DYNAMIC_DRAW);
}

void CSpriteBaseBatch::SetBatchPositions(const u32 aIndex, const glm::vec2& aPosition)
{
	D5_QUAD<D5_T2F_V3F>	localQuadData;

	if (aIndex < m_BatchSize)
	{
		localQuadData = m_QuadData;
		//Modificamos las coordenadas x,y de todos los vertices para la nueva posicion, siempre relativa al x,y del basenode.
		localQuadData.m_TopLeft.m_vertex.x += 	aPosition.x;
		localQuadData.m_TopLeft.m_vertex.y += 	aPosition.y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_TopLeft.m_vertex.z += ((f32)aIndex / 10000.0f);
		localQuadData.m_BottomLeft.m_vertex.x += 	aPosition.x;
		localQuadData.m_BottomLeft.m_vertex.y += 	aPosition.y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_BottomLeft.m_vertex.z += ((f32)aIndex / 10000.0f);
		localQuadData.m_BottomRight.m_vertex.x += 	aPosition.x;
		localQuadData.m_BottomRight.m_vertex.y += 	aPosition.y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_BottomRight.m_vertex.z += ((f32)aIndex / 10000.0f);
		localQuadData.m_TopRight.m_vertex.x += 	aPosition.x;
		localQuadData.m_TopRight.m_vertex.y += 	aPosition.y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_TopRight.m_vertex.z += ((f32)aIndex / 10000.0f);

		m_VBO[0]->UpdateBufferDataElements(&localQuadData, 1, aIndex);
	}
}

void CSpriteBaseBatch::SetBatchPositions(const std::vector<glm::vec2>& aPositions)
{
	D5_QUAD<D5_T2F_V3F>	localQuadData;

	for ( u32 count = 0; count < aPositions.size() && count < m_BatchSize; count++)
	{
		localQuadData = m_QuadData;
		//Modificamos las coordenadas x,y de todos los vertices para la nueva posicion, siempre relativa al x,y del basenode.
		localQuadData.m_TopLeft.m_vertex.x += 	aPositions[count].x;
		localQuadData.m_TopLeft.m_vertex.y += 	aPositions[count].y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_TopLeft.m_vertex.z += ((f32)count / 10000.0f);
		localQuadData.m_BottomLeft.m_vertex.x += 	aPositions[count].x;
		localQuadData.m_BottomLeft.m_vertex.y += 	aPositions[count].y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_BottomLeft.m_vertex.z += ((f32)count / 10000.0f);
		localQuadData.m_BottomRight.m_vertex.x += 	aPositions[count].x;
		localQuadData.m_BottomRight.m_vertex.y += 	aPositions[count].y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_BottomRight.m_vertex.z += ((f32)count / 10000.0f);
		localQuadData.m_TopRight.m_vertex.x += 	aPositions[count].x;
		localQuadData.m_TopRight.m_vertex.y += 	aPositions[count].y;
		if (m_BatchMode == BATCHMODE_DEPTH) localQuadData.m_TopRight.m_vertex.z += ((f32)count / 10000.0f);

		m_VBO[0]->UpdateBufferDataElements(&localQuadData, 1, count);
	}
}
