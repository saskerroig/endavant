/*
 * CTextTTF.cpp
 *
 *  Created on: 02/07/2013
 *      Author: Dani
 */

#include "CTextTTF.h"

CTextTTF::CTextTTF()
{
	m_TTFont = nullptr;
}

void CTextTTF::GetTextTTF(const std::string &aID_TTF)
{
	m_TTFont = CCoreEngine::Instance().GetResourceManager().GetFont( aID_TTF);
	if (m_TTFont)
	{
		m_GLTexture = new CGLTexture;
	}
}

void CTextTTF::InitTextTTF(const std::string &aPathToTTF, const u32 &aSize)
{
	m_TTFont = CCoreEngine::Instance().GetResourceManager().LoadFont( aPathToTTF, aSize);
	if (m_TTFont)
	{
		m_GLTexture = new CGLTexture;
	}
}

void CTextTTF::SetTextTTF(const std::string& aText, const EV_Color &aColor, const TTextureAlign aAlign)
{
	if (m_TTFont)
	{
		SDL_Surface *l_surface = m_TTFont->GetTextSurface(aText, aColor);
		LoadTextureFromSurface(*l_surface, aAlign);
		SDL_FreeSurface(l_surface);
	}
}

CTextTTF::~CTextTTF()
{
	if (m_GLTexture)
	{
		delete m_GLTexture;
		m_GLTexture = nullptr;
	}
	if (m_TTFont)
	{
		CCoreEngine::Instance().GetResourceManager().ReleaseFont( m_TTFont);
		m_TTFont = nullptr;
	}
}

