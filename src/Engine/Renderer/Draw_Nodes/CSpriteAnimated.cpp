#include "CSpriteAnimated.h"
#include "GLLoad/GLFuncs.h"

#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Utils/CXMLParserPUGI.h"
#include "Utils/CConversions.h"

CSpriteAnimated::CSpriteAnimated():
CSpriteBase()
{
	m_TimerID = CTimeManager::INVALID_TIMERID;
	m_speedAnimation = 0.1f;
}

void CSpriteAnimated::InitSpriteAnimated(const std::string& aPathToTexture, const u32 aNumFrames, const f32 aSpeed)
{
	if ( !GetFrameDataFromXml( aPathToTexture) )
	{
		LoadSpriteFromFile(aPathToTexture, aNumFrames, 1, aNumFrames);
	}

	m_speedAnimation = aSpeed;
	CreateTimer();
}

void CSpriteAnimated::InitSpriteAnimated(const std::string& aPathToTexture, const u32 aCols, const u32 aRows, const u32 aNumFrames, const f32 aSpeed)
{
	if ( !GetFrameDataFromXml( aPathToTexture) )
	{
		LoadSpriteFromFile(aPathToTexture, aCols, aRows, aNumFrames);
	}
	m_speedAnimation = aSpeed;
	CreateTimer();
}

bool CSpriteAnimated::LoadSpriteFromFile(const std::string& aPath, const u32 aCols, const u32 aRows, const u32 aNumFrames, const u32 aWidth, const u32 aHeight )
{
	m_GLTexture = CCoreEngine::Instance().GetResourceManager().LoadTexture( aPath);
	if (m_GLTexture)
	{
		u32 ActualCol = 0;
		u32 ActualRow = 0;

		f32 ColSize = ((f32)m_GLTexture->GetTextureRawWidth() / (f32)aCols) / (f32)m_GLTexture->GetTextureRawWidth();
		f32 RowSize = ((f32)m_GLTexture->GetTextureRawHeight() / (f32)aRows) / (f32)m_GLTexture->GetTextureRawHeight();

		if ( (aWidth > 0) && (aHeight > 0) )
		{
			m_SpriteSize.x = aWidth;
			m_SpriteSize.y = aHeight;
		}
		else
		{
			m_SpriteSize.x = ColSize * m_GLTexture->GetTextureRawWidth();
			m_SpriteSize.y = RowSize * m_GLTexture->GetTextureRawHeight();
		}
		EraseVBOData();
		while (	m_VBO.size() < aNumFrames )
		{
			//u-v calculations
			f32 u_i = ColSize * ActualCol;
			f32 u_f = ColSize * (ActualCol + 1);
			f32 v_i = RowSize * ActualRow;
			f32 v_f = RowSize * (ActualRow + 1);

			SetVBODataAligned( TEXTUREALIGN_CENTER, (m_VBO.size()), 0, 0, u_i, u_f, v_i, v_f );

			ActualCol++;
			if ( ActualCol >= aCols )
			{
				ActualCol = 0;
				ActualRow++;
			}
		}
	}
	return (m_GLTexture?true:false);
}

bool CSpriteAnimated::LoadSpriteFromFile(const std::string& aPath, const u32 aNumFrames, const std::vector<glm::vec4>& aFrameData)
{
	if (aFrameData.size() < aNumFrames )
	{
		return false;
	}

	m_GLTexture = CCoreEngine::Instance().GetResourceManager().LoadTexture( aPath);
	if (m_GLTexture)
	{
		EraseVBOData();
		while (	m_VBO.size() < aNumFrames )
		{
			u32 NumFrame = m_VBO.size();

			//u-v calculations
			f32 u_i = ((f32)aFrameData[NumFrame].x) / (f32)m_GLTexture->GetTextureRawWidth();
			f32 u_f = ((f32)aFrameData[NumFrame].z) / (f32)m_GLTexture->GetTextureRawWidth();
			f32 v_i = ((f32)aFrameData[NumFrame].y) / (f32)m_GLTexture->GetTextureRawHeight();
			f32 v_f = ((f32)aFrameData[NumFrame].w) / (f32)m_GLTexture->GetTextureRawHeight();

			SetVBODataAligned( TEXTUREALIGN_CENTER, NumFrame, (aFrameData[NumFrame].z - aFrameData[NumFrame].x), (aFrameData[NumFrame].w - aFrameData[NumFrame].y), u_i, u_f, v_i, v_f );
		}
	}

	return (m_GLTexture?true:false);
}

void CSpriteAnimated::Render()
{
	// Check if the texture is loaded
	if ( m_GLTexture->IsLoaded() && m_Visible )
	{
		glLoadIdentity();
		glTranslatef( m_PositionAbsolute.x, m_PositionAbsolute.y, m_PositionAbsolute.z);
		glRotatef( GetRotation() ,0.0f,0.0f,1.0f);

		m_GLTexture->Bind();
		m_VBO[m_FrameNum]->BindBuffer();
		glTexCoordPointer(2,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0) );
		glVertexPointer(3,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0 + (2*sizeof(GLfloat))) );

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glDrawArrays(GL_TRIANGLE_STRIP,0,4);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		m_VBO[m_FrameNum]->UnBindBuffer();
		m_GLTexture->UnBind();

	}
	CSpriteBase::Render();
}

void CSpriteAnimated::Update(f64 dt)
{
	if (m_TimerID)
	{
		if ( CCoreEngine::Instance().GetTimerManager().IsEndTimer(m_TimerID) )
		{
			FrameIncrement();
		}
	}
	CSpriteBase::Update( dt);
}

CSpriteAnimated::~CSpriteAnimated()
{
}

bool CSpriteAnimated::GetFrameDataFromXml( const std::string& aPathToTexture)
{
	bool retValue = false;
	std::string PathToImage;
	std::vector<glm::vec4> FrameData;

	std::size_t found = aPathToTexture.find(".xml");
	if (found!=std::string::npos)
	{
		CXMLParserPUGI Parser( aPathToTexture );
		if (!Parser.Ready())
		{
			LOG( LOG_ERROR, LOGSUB_RESOURCES, "FAILED LOADING %s", aPathToTexture.c_str() );
			return retValue;
		}
		std::size_t founddir = aPathToTexture.rfind("/");
		if (founddir!=std::string::npos)
		{
			PathToImage = aPathToTexture.substr(0,(founddir+1));
		}
		std::string l_File = Parser.GetStringAttributeValue( "TextureAtlas", "imagePath");
		PathToImage += l_File;

		u32 FramesCount = Parser.GetNodeNameCount("TextureAtlas/sprite");
		for ( u32 c = 0; c < FramesCount; c++ )
		{
			std::string XmlNode("TextureAtlas/sprite#");
			XmlNode += to_string( c );

			s32 l_Xi = Parser.GetIntAttributeValue( XmlNode, "x");
			s32 l_Yi = Parser.GetIntAttributeValue( XmlNode, "y");
			s32 l_Xf = l_Xi + Parser.GetIntAttributeValue( XmlNode, "w");
			s32 l_Yf = l_Yi + Parser.GetIntAttributeValue( XmlNode, "h");

			FrameData.push_back( glm::vec4(l_Xi,l_Yi,l_Xf,l_Yf) );
		}

		LoadSpriteFromFile(PathToImage, FrameData.size(), FrameData );
		retValue = true;
	}

	return retValue;
}

void CSpriteAnimated::CreateTimer()
{
	auto& TimeManager = CCoreEngine::Instance().GetTimerManager();

	if (m_TimerID != CTimeManager::INVALID_TIMERID)
		TimeManager.KillTimer(m_TimerID);

	m_TimerID  = TimeManager.CreateTimer(m_speedAnimation,true );
}
