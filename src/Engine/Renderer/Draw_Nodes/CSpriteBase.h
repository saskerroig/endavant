#ifndef CSPRITEBASE_H_
#define CSPRITEBASE_H_

#include "Renderer/Base_Nodes/CBaseNode.h"
#include "Renderer/Draw_Nodes/CSpriteAlign.h"

#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

// OpenGL Objects
#include "Renderer/GL/CGLTexture.h"
#include "Renderer/GL/CGLBufferObject.h"
#include "GLLoad/GLFuncs.h"

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


class CSpriteBase: public CBaseNode
{
public:
	CSpriteBase();
	virtual 			~CSpriteBase();
	virtual void		Render();
	virtual void		Update(f64 dt);

	bool				LoadSpriteFromFile(const std::string &aPath, const u32 aWidth = 0, const u32 aHeight = 0  );

	// Current Size
	inline u32			GetSpriteWidth() const { return m_SpriteSize.x; }
	inline u32			GetSpriteHeight() const { return m_SpriteSize.y; }
	inline glm::uvec2	GetSpriteSize() const { return m_SpriteSize; }
	inline void			SetSpriteWidth(const u32 aWidth) { m_SpriteSize.x = aWidth; }
	inline void			SetSpriteHeight(const u32 aHeight) { m_SpriteSize.y = aHeight; }
	inline void			SetSpriteSize(const glm::uvec2 &aSize) { m_SpriteSize = aSize; }

	// Original Size
	inline void 		ResetSpriteSize() { m_SpriteSize = m_GLTexture->GetTextureRawSize(); SetVBOData( TEXTUREALIGN_CENTER ); }

protected:
	u32					m_FrameNum;
	std::vector< CGLBufferObject< D5_QUAD<D5_T2F_V3F> > * >	m_VBO;
	CGLTexture			*m_GLTexture;
	glm::uvec2			m_SpriteSize;
	D5_QUAD<D5_T2F_V3F>	m_QuadData;

	bool				LoadTextureFromSurface(const SDL_Surface &aSurface, const TTextureAlign aAlign = TEXTUREALIGN_CENTER ); // Convert SDL_Surface and upload texture data to the GPU
	void				SetVBOData( const u32 aFrame = 0, const f32 aX_i = 0.0f, const f32 aX_f = 1.0f, const f32 aY_i = 0.0f, const f32 aY_f = 1.0f, const f32 aU_i = 0.0f, const f32 aU_f = 1.0f, const f32 aV_i = 0.0f, const f32 aV_f = 1.0f );
	void				SetVBODataAligned( const TTextureAlign aAlign = TEXTUREALIGN_CENTER, const u32 aFrame = 0, const u32 aW = 0, const u32 aH = 0, const f32 aU_i = 0.0f, const f32 aU_f = 1.0f, const f32 aV_i = 0.0f, const f32 aV_f = 1.0f );	// Upload Vertex Data to the GPU, Aligned
	void				EraseVBOData();
	void				AddVBOData();
};

#endif /* CSPRITEBASE_H_ */
