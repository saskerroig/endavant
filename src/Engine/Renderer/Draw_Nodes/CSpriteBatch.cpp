#include "CSpriteBatch.h"
#include "GLLoad/GLFuncs.h"


CSpriteBatch::CSpriteBatch():
CSpriteBaseBatch(){
}

void CSpriteBatch::InitSprite(const std::string& aPathToTexture)
{
	LoadSpriteFromFile(aPathToTexture);
}

CSpriteBatch::~CSpriteBatch()
{
}

void CSpriteBatch::Render()
{
	// Check if the texture is loaded
	if ( m_GLTexture->IsLoaded() && m_Visible )
	{
		glLoadIdentity();
		glTranslatef( m_PositionAbsolute.x, m_PositionAbsolute.y, m_PositionAbsolute.z);
		glRotatef( GetRotation() ,0.0f,0.0f,1.0f);

		m_GLTexture->Bind();
		m_VBO[0]->BindBuffer();
		glTexCoordPointer(2,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0) );
		glVertexPointer(3,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0 + (2*sizeof(GLfloat))) );

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		if (m_BatchMode == BATCHMODE_NODEPTH) glDisable(GL_DEPTH_TEST);
		//Esto deberia poder hacerse con una unica llamada, la documentacion no es clara en como utilizar bloques de 4 vertices para ir dibujando QUADS, google esta lleno de dudas de la gente
		//La solucion facil son multillamadas
		for (u32 count = 0; count < m_BatchSize; count++)
		{
			glDrawArrays(GL_TRIANGLE_STRIP,4*count,4);
		}
		if (m_BatchMode == BATCHMODE_NODEPTH) glEnable(GL_DEPTH_TEST);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		m_VBO[0]->UnBindBuffer();
		m_GLTexture->UnBind();

	}
	CSpriteBaseBatch::Render();
}

void CSpriteBatch::Update(f64 dt)
{
	CSpriteBaseBatch::Update( dt);
}

