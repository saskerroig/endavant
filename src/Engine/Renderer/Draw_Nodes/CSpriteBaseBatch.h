#ifndef CSPRITEBASEBATCH_H_
#define CSPRITEBASEBATCH_H_

#include "Renderer/Draw_Nodes/CSpriteBase.h"

enum TBatchMode
{
	BATCHMODE_NODEPTH = 0x00,
	BATCHMODE_DEPTH	,
};

class CSpriteBaseBatch: public CSpriteBase
{
public:
	CSpriteBaseBatch();
	virtual 			~CSpriteBaseBatch();
	virtual void		Render();
	virtual void		Update(f64 dt);

	inline void			SetBatchMode(const TBatchMode aMode) { m_BatchMode = aMode; };
	void				SetBatchSize(const u32 aSize);
	//BatchPositions are relative to BaseNode Position. 1st position is a "NormalSprite" position, cannot modify it.
	void				SetBatchPositions( const u32 aIndex, const glm::vec2 &aPosition );
	void				SetBatchPositions( const std::vector<glm::vec2> &aPositions );
protected:
	TBatchMode				m_BatchMode;
	u32						m_BatchSize;
	std::vector<glm::uvec2>	m_BatchPositions;
};

#endif /* CSPRITEBASEBATCH_H_ */
