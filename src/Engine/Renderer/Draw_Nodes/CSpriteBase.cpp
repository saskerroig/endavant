#include "CSpriteBase.h"

// TODO: mipmaps?


CSpriteBase::CSpriteBase():
CBaseNode(),
m_FrameNum(0),
m_SpriteSize(0,0)
{
	m_GLTexture = nullptr;
	EraseVBOData();
}

CSpriteBase::~CSpriteBase()
{
	if (m_GLTexture)
	{
		CCoreEngine::Instance().GetResourceManager().ReleaseTexture( m_GLTexture);
		m_GLTexture = nullptr;
	}
}

bool CSpriteBase::LoadSpriteFromFile(const std::string& aPath, const u32 aWidth, const u32 aHeight)
{
	m_GLTexture = CCoreEngine::Instance().GetResourceManager().LoadTexture( aPath);
	if (m_GLTexture)
	{
		if ( (aWidth > 0) && (aHeight > 0) )
		{
			m_SpriteSize.x = aWidth;
			m_SpriteSize.y = aHeight;
		}
		else
		{
			m_SpriteSize = m_GLTexture->GetTextureRawSize();
		}
		SetVBODataAligned( TEXTUREALIGN_CENTER );
	}

	return (m_GLTexture?true:false);
}

bool CSpriteBase::LoadTextureFromSurface(const SDL_Surface &aSurface, const TTextureAlign aAlign)
{
	bool retval = false;

	if (m_GLTexture)
	{
		retval = m_GLTexture->LoadTextureFromSurface(aSurface);
		//Forzamos el tama�o de la textura a partir del Surface (necesario para Texto)
		m_SpriteSize.x = aSurface.w;
		m_SpriteSize.y = aSurface.h;
		if (retval)
		{
			EraseVBOData();

			SetVBODataAligned( aAlign );
		}
	}
	return retval;
}

void CSpriteBase::EraseVBOData()
{
	for (auto &VBOElement: m_VBO)
	{
		delete VBOElement;
	}

	m_VBO.clear();
}

void CSpriteBase::AddVBOData()
{
	CGLBufferObject< D5_QUAD<D5_T2F_V3F> > *m_VBO_Element = new CGLBufferObject< D5_QUAD<D5_T2F_V3F> >(GL_ARRAY_BUFFER);
	m_VBO.push_back( m_VBO_Element );
}


void CSpriteBase::SetVBOData(const u32 aFrame, const f32 aX_i, const f32 aX_f, const f32 aY_i, const f32 aY_f,  const f32 aU_i, const f32 aU_f, const f32 aV_i, const f32 aV_f )
{
	AddVBOData();

	m_QuadData.m_TopLeft 		=	D5_T2F_V3F(glm::vec2(aU_i,aV_i), glm::vec3(aX_i,aY_f,1));
	m_QuadData.m_BottomLeft 	=	D5_T2F_V3F(glm::vec2(aU_i,aV_f), glm::vec3(aX_i,aY_i,1));
	m_QuadData.m_BottomRight 	=	D5_T2F_V3F(glm::vec2(aU_f,aV_i), glm::vec3(aX_f,aY_f,1));
	m_QuadData.m_TopRight 		=	D5_T2F_V3F(glm::vec2(aU_f,aV_f), glm::vec3(aX_f,aY_i,1));

	m_VBO[aFrame]->LoadBufferData(&m_QuadData, 1, GL_STATIC_DRAW);

}

void CSpriteBase::SetVBODataAligned(const TTextureAlign aAlign, const u32 aFrame, const u32 aW, const u32 aH,  const f32 aU_i, const f32 aU_f, const f32 aV_i, const f32 aV_f )
{
	const f32 w = (aW==0?m_SpriteSize.x:aW);
	const f32 h = (aH==0?m_SpriteSize.y:aH);
	f32 aX_i, aX_f, aY_i, aY_f;

	switch ( (aAlign & 0x0F) )	// Horizontal
	{
		case TEXTUREALIGN_LEFT:
		{
			aX_i = 0;
			aX_f = w;
			break;
		}
		case TEXTUREALIGN_RIGHT:
		{
			aX_i = w * -1;
			aX_f = 0;
			break;
		}
		case TEXTUREALIGN_CENTER:
		default:
		{
			aX_i = (w/2) * -1;
			aX_f = (w/2) * +1;
			break;
		}
	}
	switch ( (aAlign & 0xF0) )	// Vertical
	{
		case TEXTUREVALIGN_BOTTOM:
		{
			aY_i = 0;
			aY_f = h;
			break;
		}
		case TEXTUREVALIGN_TOP:
		{
			aY_i = h * -1;
			aY_f = 0;
			break;
		}
		case TEXTUREVALIGN_MIDDLE:
		default:
		{
			aY_i = (h/2) * -1;
			aY_f = (h/2) * +1;
			break;
		}
	}
	SetVBOData( aFrame, aX_i, aX_f, aY_i, aY_f, aU_i, aU_f, aV_i, aV_f );
}

void CSpriteBase::Render()
{
	CBaseNode::Render();
}

void CSpriteBase::Update(f64 dt)
{
	CBaseNode::Update( dt);
}
