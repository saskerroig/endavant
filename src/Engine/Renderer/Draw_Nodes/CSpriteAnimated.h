#ifndef CSPRITEANIMATED_H_
#define CSPRITEANIMATED_H_
#include "Renderer/Draw_Nodes/CSpriteBase.h"
#include "Time/CTimeManager.h"

class CSpriteAnimated : public CSpriteBase
{
public:
	CSpriteAnimated();
	virtual ~CSpriteAnimated();

	virtual void Render();
	virtual void Update(f64 dt);

	void		InitSpriteAnimated(const std::string &aPathToTexture, const u32 aNumFrames = 1, const f32 aSpeed = 0.1f );
	void		InitSpriteAnimated(const std::string &aPathToTexture, const u32 aCols, const u32 aRows, const u32 aNumFrames, const f32 aSpeed = 0.1f);

	inline void	FrameSet( const u32 aFrameNum  ) { m_FrameNum = aFrameNum; }
	inline void	FrameIncrement() { m_FrameNum = (m_FrameNum >= (m_VBO.size() - 1) ? 0 : (m_FrameNum + 1) ); }
	inline void	FrameDecrement() { m_FrameNum = (m_FrameNum < 1 ? (m_VBO.size() - 1) :(m_FrameNum - 1) ); }

private:
	bool		LoadSpriteFromFile(const std::string &aPath, const u32 aCols, const u32 aRows, const u32 aNumFrames, const u32 aWidth = 0, const u32 aHeight = 0 );
	bool		LoadSpriteFromFile(const std::string &aPath, const u32 aNumFrames, const std::vector<glm::vec4> &aFrameData );
	bool		GetFrameDataFromXml( const std::string &aPathToTexture);
	void		CreateTimer();

	f32			m_speedAnimation;
	EV_TimerID	m_TimerID;
};

#endif /* CSPRITEANIMATED_H_ */
