#include "CSprite.h"
#include "GLLoad/GLFuncs.h"


CSprite::CSprite():
CSpriteBase(){
}

void CSprite::InitSprite(const std::string& aPathToTexture)
{
	LoadSpriteFromFile(aPathToTexture);
}

CSprite::~CSprite()
{
}

void CSprite::Render()
{
	// Check if the texture is loaded
	if ( m_GLTexture->IsLoaded() && m_Visible )
	{
		glLoadIdentity();
		glTranslatef( m_PositionAbsolute.x, m_PositionAbsolute.y, m_PositionAbsolute.z);
		glRotatef( GetRotation() ,0.0f,0.0f,1.0f);

		m_GLTexture->Bind();
		m_VBO[0]->BindBuffer();
		glTexCoordPointer(2,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0) );
		glVertexPointer(3,GL_FLOAT,5 * sizeof(GLfloat), ((GLubyte *) 0 + (2*sizeof(GLfloat))) );

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glDrawArrays(GL_TRIANGLE_STRIP,0,4);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		m_VBO[0]->UnBindBuffer();
		m_GLTexture->UnBind();

	}
	CSpriteBase::Render();
}

void CSprite::Update(f64 dt)
{
	CSpriteBase::Update( dt);
}

