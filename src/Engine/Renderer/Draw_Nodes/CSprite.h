#ifndef CSPRITE_H_
#define CSPRITE_H_
#include "Renderer/Draw_Nodes/CSpriteBase.h"


class CSprite : public CSpriteBase
{
public:
	CSprite();
	virtual ~CSprite();

	virtual void Render();
	virtual void Update(f64 dt);

	void	InitSprite(const std::string &aPathToTexture);


private:

};

#endif /* CSPRITE_H_ */
