/*
 * CGLTexture.h
 *
 *  Created on: 29/01/2013
 *      Author: dani
 */

#ifndef CGLTEXTURE_H_
#define CGLTEXTURE_H_

#include "GLLoad/GLFuncs.h"
#include "Renderer/GL/CGLTypes.h"
#include "Core/CBasicTypes.h"
#include <string>
#include <SDL2/SDL.h>

// http://www.opengl.org/sdk/docs/man/xhtml/glTexImage2D.xml

class CGLTexture
{
public:
	CGLTexture();
	virtual ~CGLTexture();

	bool	LoadTextureFromFile(const std::string& aPath);
	bool	LoadTextureFromSurface(const SDL_Surface &aSurface);
	void 	UnLoad();

	void	Bind();
	void	UnBind();

	bool	IsLoaded();

	inline const std::string &GetTextureName() const { return m_TextureName; }
	inline u32			GetTextureRawWidth() const { return m_TextureRawSize.x; }
	inline u32			GetTextureRawHeight() const { return m_TextureRawSize.y; }
	inline glm::uvec2	GetTextureRawSize() const { return m_TextureRawSize; }

private:
	void	InitWithData(const GLvoid* aRawDataPixels, GLint aInternalFormat, GLsizei aWidth, GLsizei aHeight, GLenum aFormat);

	GLuint 		m_TextureID;
	glm::uvec2	m_TextureRawSize;
public:
	std::string m_TextureName;
	s32			m_RefCount;
};

#endif /* CGLTEXTURE_H_ */
