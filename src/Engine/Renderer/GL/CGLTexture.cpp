
#include "CGLTexture.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CGLTexture::CGLTexture():
m_TextureID(GL_ZERO),
m_TextureName(""),
m_RefCount(1)
{
}


void CGLTexture::InitWithData(const GLvoid* aRawDataPixels, GLint aInternalFormat,GLsizei aWidth, GLsizei aHeight, GLenum aFormat)
{
	UnLoad();
	glGenTextures(1, &m_TextureID);


	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, aInternalFormat, aWidth, aHeight, 0,aFormat,GL_UNSIGNED_BYTE, aRawDataPixels);
	UnBind();

}

bool CGLTexture::LoadTextureFromFile(const std::string& aPath)
{
	SDL_Surface* Surface = IMG_Load(aPath.c_str());
	if (!Surface)
	{
		LOG( LOG_ERROR, LOGSUB_RESOURCES, "FAILED LOADING %s", aPath.c_str() );
		return false;
	}

	m_TextureName = aPath;
	bool retval = LoadTextureFromSurface(*Surface);
	SDL_FreeSurface(Surface);

	return retval;
}

bool CGLTexture::LoadTextureFromSurface(const SDL_Surface &aSurface)
{
	GLenum TextureFormat;

	switch(aSurface.format->BytesPerPixel)
	{
	case 3:
		if (aSurface.format->Rmask == 0x000000ff)
			TextureFormat = GL_RGB;
		else
			TextureFormat = GL_BGR;
		break;

	case 4:
		if (aSurface.format->Rmask == 0x000000ff)
			TextureFormat = GL_RGBA;
		else
			TextureFormat = GL_BGRA;
		break;

	default:
		return false;
	}

	m_TextureRawSize.x = aSurface.w;
	m_TextureRawSize.y = aSurface.h;
	InitWithData(aSurface.pixels,aSurface.format->BytesPerPixel,m_TextureRawSize.x,m_TextureRawSize.y,TextureFormat);

	return true;
}

void CGLTexture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, m_TextureID);
}

void CGLTexture::UnBind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CGLTexture::UnLoad()
{
	if (m_TextureID != GL_ZERO)
	{
		glDeleteTextures(1, &m_TextureID);
		m_TextureID = GL_ZERO;
	}
}

CGLTexture::~CGLTexture()
{
	UnLoad();
}

bool	CGLTexture::IsLoaded()
{
	if (m_TextureID == GL_ZERO)
		return false;
	else
		return true;
}

