#ifndef CSOUND_EFFECT_H_
#define CSOUND_EFFECT_H_

#include "SDL2/SDL_mixer.h"
#include "Core/CBasicTypes.h"

#include <string>

class CSoundEffect
{
public:
	CSoundEffect(const std::string& a_FilePath);

	bool	Load();
	void 	UnLoad();
	bool 	IsLoaded();

	void 	SetVolume(const u32 a_Volume);
	u32 	GetVolume();


	void	Play();
	void	Stop();
	void	Pause();
	void	Resume();


	~CSoundEffect();

private:
	//Private constructors
	CSoundEffect(const CSoundEffect& a_Other);
	CSoundEffect();

	Mix_Chunk* 	m_Chunk;
	s32			m_Volume;
	std::string	m_Path;
	s32			m_Channel;
};

#endif

