#include "CMusicTrack.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

// Init private static variable
s32 CMusicTrack::s_Volume = MIX_MAX_VOLUME;

CMusicTrack::CMusicTrack(const std::string& a_ID, const std::string& a_Path):
	m_Music(nullptr),
	m_Path(a_Path)
{
}

CMusicTrack::~CMusicTrack()
{
	UnLoad();
}

bool CMusicTrack::Load()
{
	if (m_Music != nullptr)
		return false;

	m_Music = Mix_LoadMUS(m_Path.c_str());

	if (!m_Music)
	{
		LOG(LOG_ERROR, LOGSUB_SOUND, "CMusicTrack::Load Error: %s", Mix_GetError());
		return false;
	}

	return true;
}

void CMusicTrack::UnLoad()
{
	if (m_Music != nullptr)
	{
		Mix_FreeMusic(m_Music);
		m_Music = nullptr;
	}

}

bool CMusicTrack::IsLoaded() const
{
	return (m_Music != nullptr);
}

void CMusicTrack::Play()
{
	// If music is not pre-loaded, load it
	if (m_Music == nullptr)
		if ( !Load() )
			return;

	// Stop the current music
	Stop();

	// Play the music Mix_PlayMusic:
	// 0 plays the music zero times...
	//-1 plays the music forever (or as close as it can get to that)
	if (Mix_PlayMusic(m_Music, -1) == -1)
		LOG(LOG_ERROR, LOGSUB_SOUND, "CMusicTrack::Play Error: %s", Mix_GetError());

}

void CMusicTrack::Stop()
{
	Mix_HaltMusic();
}

void CMusicTrack::Pause()
{
	Mix_PauseMusic();
}

void CMusicTrack::Resume()
{
	Mix_ResumeMusic();
}

void CMusicTrack::Rewind()
{
	Mix_RewindMusic();
}
void CMusicTrack::SetVolume(const u32 a_Volume)
{

	if (a_Volume > MIX_MAX_VOLUME)
		s_Volume = MIX_MAX_VOLUME;
	else
		s_Volume = a_Volume;

	Mix_VolumeMusic(s_Volume);

}

u32 CMusicTrack::GetVolume()
{
	return s_Volume;
}
