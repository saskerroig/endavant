#include "Resources/CResourceManager.h"
#include "CSoundManager.h"
#include "Core/CCoreEngine.h"
#include "Core/Defines.h"
#include "Utils/CXMLParserPUGI.h"
#include "SDL2/SDL_mixer.h"
#include "Core/CLogManager.h"
#include <algorithm>
#include <set>

#include "Sound/CSoundEffect.h"

static const std::string AUDIO_CONFIG_XML_PATH("./Config/audio_options.xml");


CSoundManager::CSoundManager()
{

}

CSoundManager::~CSoundManager()
{

}

void CSoundManager::StartUp(void)
{
	CXMLParserPUGI Parser(AUDIO_CONFIG_XML_PATH);

	//Initialize SDL_mixer
	if (Mix_OpenAudio(Parser.GetIntAttributeValue("AUDIO/OPTIONS", "Frequency"),
				MIX_DEFAULT_FORMAT,
				Parser.GetIntAttributeValue("AUDIO/OPTIONS", "Channels"),
				Parser.GetIntAttributeValue("AUDIO/OPTIONS", "ChunkSize")) == -1)
	{
		LOG(LOG_ERROR, LOGSUB_SOUND, "INIT SDL_MIXER ERROR: %s", Mix_GetError());
		return;
	}


	int MixingChannels = Parser.GetIntAttributeValue("AUDIO/OPTIONS", "MixingChannels");
	if (Mix_AllocateChannels(MixingChannels) != MixingChannels)
	{
		LOG(LOG_ERROR, LOGSUB_SOUND, "Error Allocating Audio Channels: %s", Mix_GetError());
		return;
	}

	Mix_ChannelFinished(nullptr);
	LOG( LOG_INFO, LOGSUB_SOUND ,"StartUp");
}

void CSoundManager::ShutDown(void)
{
	Mix_CloseAudio();
}

void CSoundManager::Update(f64 dt)
{

}

/*
CSoundEffect &CSoundManager::GetSoundEffect(const std::string& a_ID)
{
	//TODO


	return CSoundManager();
}

CMusicTrack &CSoundManager::GetMusicTrack(const std::string& a_ID)
{
	//TODO
	return CMusicTrack();
}
*/
void CSoundManager::AddSound(const std::string& a_Path, const std::string& a_ID)
{
	auto l_it = m_MapEffects.find(a_ID);
	if (l_it == m_MapEffects.end())
	{
		auto l_SoundEffect = new CSoundEffect(a_Path);
		m_MapEffects[a_ID] = l_SoundEffect;
	}
	else
		LOG(LOG_WARNING, LOGSUB_SOUND, "CSoundManager::AddSound ID %s Exists", a_ID.c_str());

}

void CSoundManager::AddMusicTrack(const std::string& a_Path,
		const std::string& a_ID)
{
}

void CSoundManager::Mute()
{
}
