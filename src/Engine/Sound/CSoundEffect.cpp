#include "CSoundEffect.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CSoundEffect::CSoundEffect(const std::string& a_Path) :
	m_Chunk(nullptr),
	m_Volume(MIX_MAX_VOLUME),
	m_Path(a_Path),
	m_Channel(-1)
{
}

void CSoundEffect::Play()
{
	// If sound is not pre-loaded, Load it
	if (m_Chunk == nullptr)
		if ( !Load() )
			return;

	// Stop sound in case this is playing
	Stop();

	// Play sound in an available channel
	m_Channel = Mix_PlayChannel( -1, m_Chunk, 0 );
	if (m_Channel == -1)
		LOG(LOG_ERROR, LOGSUB_SOUND, "CSoundEffect::Play Mix_PlayChannel: %s", Mix_GetError());

}

void CSoundEffect::Stop()
{
	if ( m_Channel != -1)
		Mix_HaltChannel(m_Channel);

	m_Channel = -1;
}

void CSoundEffect::Pause()
{
	if ( m_Channel != -1)
		Mix_Pause(m_Channel);
}

void CSoundEffect::Resume()
{
	if ( m_Channel != -1)
		Mix_Resume(m_Channel);
}

CSoundEffect::~CSoundEffect()
{
	UnLoad();
}

bool CSoundEffect::Load()
{
	if (m_Chunk != nullptr)
		return false;

	m_Chunk = Mix_LoadWAV(m_Path.c_str());

	if (!m_Chunk)
	{
		LOG(LOG_ERROR, LOGSUB_SOUND, "CSoundEffect::Load Error: %s", Mix_GetError());

		return false;
	}

	return true;
}

void CSoundEffect::UnLoad()
{
	if (m_Chunk != nullptr)
	{
		Stop();
		Mix_FreeChunk(m_Chunk);
		m_Chunk = nullptr;
	}
}

void CSoundEffect::SetVolume(const u32 a_Volume)
{
	if (a_Volume >= 0 && a_Volume <= MIX_MAX_VOLUME)
	{
		m_Volume = a_Volume;
		if (m_Chunk != nullptr)
			Mix_VolumeChunk(m_Chunk, m_Volume);
	}
}

u32 CSoundEffect::GetVolume()
{
	return m_Volume;
}

bool CSoundEffect::IsLoaded()
{
	return (m_Chunk != nullptr);
}


