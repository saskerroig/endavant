#ifndef CSOUNDMANAGER_H_
#define CSOUNDMANAGER_H_

#include "Core/ISubSystem.h"
#include "SDL2/SDL_mixer.h"

#include <map>
#include <string>

class CSoundEffect;
class CMusicTrack;


class CSoundManager: public ISubSystem
{

public:
	CSoundManager();
	virtual ~CSoundManager();

	void 			StartUp(void);
	void			ShutDown(void);
	void			Update(f64 dt);

	/*
	CSoundEffect 	&GetSoundEffect(const std::string& a_ID);
	CMusicTrack		&GetMusicTrack(const std::string& a_ID);
*/
	void			AddSound(const std::string& a_Path, const std::string& a_ID);
	void			AddMusicTrack(const std::string& a_Path, const std::string& a_ID);

	void			Mute();




private:

	typedef std::map<std::string , CSoundEffect *>	t_SoundsMap;
	typedef std::map<std::string , CMusicTrack *>	t_MusicsMap;
	t_SoundsMap				m_MapEffects;
	t_MusicsMap				m_MapMusicTracks;

};

#endif /* CSOUNDMANAGER_H_ */
