#ifndef CMUSIC_TRACK_H_
#define CMUSIC_TRACK_H_

#include <string>
#include "Core/CBasicTypes.h"
#include "SDL2/SDL_mixer.h"

class CMusicTrack
{
public:
	CMusicTrack(const std::string& a_ID, const std::string& a_Path);
	virtual ~CMusicTrack();

	bool			Load();
	void 			UnLoad();
	bool 			IsLoaded() const;
	void			Play();
	static void		Stop();
	static void		Pause();
	static void		Resume();
	static void		Rewind();
	static void 	SetVolume(const u32 a_Volume);
	static u32 		GetVolume();

private:
	CMusicTrack(const CMusicTrack& );
	CMusicTrack();

	Mix_Music* 	m_Music;
	std::string	m_Path;
	static s32	s_Volume;
};

#endif


