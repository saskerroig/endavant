#include <thread>
#include <mutex>
#include "Core/CCoreEngine.h"
#include "net/manager.h"
#include "net/socket.h"

const std::string NET_CONFIG_XML_PATH("./Config/net_options.xml");
const std::string SERVER_XML_OPTIONS("NET/SERVER/OPTIONS/OPTION");
const s32 OPTIONS_DEFAULT_VALUE = -1;

namespace edv
{
namespace net
{

manager::manager() :
	srv_options_ids_(NET_CONFIG_XML_PATH, SERVER_XML_OPTIONS)
{
	load_options();
	socket::init();
}

manager::~manager()
{
	socket::stop(); //#todo: hay q asegurarse que se llama despues de destruir tcp::server, etc.
}

void manager::StartUp()
{

}

void manager::ShutDown()
{

}

void manager::Update(f64 dt)
{

}

s32 manager::srv_option(NET_SRV_OPTION option)
{
	std::lock_guard<std::mutex> lock(srv_opts_mutex_);

	auto it = srv_options_.find(option);

	if (it != srv_options_.end())
		return it->second;

	return 0; //@TODO: excepcion
}

void manager::load_options()
{
	CXMLParserPUGI parser(NET_CONFIG_XML_PATH);

	//@TODO: mejorar; xml_enum podria ser menos generico y contener el la asociacion opcion,valor
	auto srv_keys = srv_options_ids_.strings();
	for (u32 index = 1; index < NET_SRV_OPT_ALL; index++)
	{
		s32 value = parser.GetIntAttributeValue(SERVER_XML_OPTIONS + "#" + to_string(index-1), "value", OPTIONS_DEFAULT_VALUE);

		if (value == OPTIONS_DEFAULT_VALUE)
			continue; //@TODO: excepcion

		NET_SRV_OPTION option = static_cast<NET_SRV_OPTION>(index);
		srv_options_[option] = value;
		LOG(LOG_INFO, LOGSUB_NETWORK, "server option: %s\t=\t%d", srv_options_ids_[option].c_str(), value);
	}
}

} /* namespace net */
} /* namespace edv */
