#include "Core/CCoreEngine.h"
#include "net/socket.h"

namespace edv		{
namespace net		{
namespace socket	{

bool init()
{
#if defined(EDV_WINDOWS)
	WSADATA	data;

	if (WSAStartup(MAKEWORD(2,2), &data) != 0)
		return false;
#endif
	return true;
}

void stop()
{
#ifdef EDV_WINDOWS
	WSACleanup();
#endif
}

socket_t invalid()
{
#ifdef EDV_LINUX
	return EDVSOCKETERROR;
#elif defined(EDV_WINDOWS)
	return EDVINVALIDSOCK;
#endif
}

void close_socket(socket_t sck)
{
#ifdef EDV_LINUX
	close(sck);
#elif defined(EDV_WINDOWS)
	closesocket(sck);
#endif
}

void set_blocking(socket_t sck)
{
#ifdef EDV_LINUX
	fcntl(sck, F_SETFL, fcntl(sck, F_GETFL) & ~O_NONBLOCK);
#elif defined(EDV_WINDOWS)
	u_long mode = 0;
	ioctlsocket(sck, FIONBIO, &mode);
#endif
}

void set_non_blocking(socket_t sck)
{
#ifdef EDV_LINUX
	fcntl(sck, F_SETFL, fcntl(sck, F_GETFL) | O_NONBLOCK);
#elif defined(EDV_WINDOWS)
	u_long mode = 1;
	ioctlsocket(sck, FIONBIO, &mode);
#endif
}

bool set_reuse(socket_t sck)
{
#ifdef EDV_LINUX
	const s32 reuse = 1;
#elif defined(EDV_WINDOWS)
	const char reuse = 1;
#endif
	return setsockopt(sck, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) != EDVSOCKETERROR;
}

int last_error()
{
#ifdef EDV_LINUX
	return errno;
#elif defined(EDV_WINDOWS)
	return WSAGetLastError();
#endif
}

s32 waiting_bytes(socket_t sck)
{
	s32 ret = 0;

#ifdef EDV_LINUX
	s32 bytes = 0;
	ret = ioctl(sck, FIONREAD, &bytes);
#elif defined(EDV_WINDOWS)
	u_long bytes = 0;
	ret = ioctlsocket(sck, FIONREAD, &bytes);
#endif

	if (ret == EDVSOCKETERROR)
		LOG(LOG_WARNING, LOGSUB_NETWORK, "ioctl: %s", strerror(last_error()));

	return bytes;

}

}
}
}
