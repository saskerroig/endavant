#ifndef EDV_NET_MSG_HANDLER_H_
#define EDV_NET_MSG_HANDLER_H_

#include "net/buffer.h"

namespace edv {
namespace net {

class msg_handler
{
public:
	virtual ~msg_handler() { }

	virtual void process_message(buffer<>& in, buffer<>* out) = 0;
};

}
}

#endif
