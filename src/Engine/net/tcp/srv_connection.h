#ifndef TCP_SERVER_CONNECTION_H_
#define TCP_SERVER_CONNECTION_H_

#include <string>
#include "Core/CBasicTypes.h"
#include "net/socket.h"

namespace edv {
namespace net {
namespace tcp {

class srv_connection
{
public:
	srv_connection(socket::socket_t sck, const sockaddr_storage& addr);
	virtual ~srv_connection();

	socket::socket_t	socket()	const;
	const std::string&	ip()		const;
	socket::port_t		port()		const;
	bool				timeout()	const;
	void				update();

private:
	socket::socket_t	socket_;
	std::string			ip_;
	s32					cli_timeout_;
	time_t				timeout_check_;
	socket::port_t		port_;
};

}
}
}
#endif /* TCPSERVERCLIENT_H_ */
