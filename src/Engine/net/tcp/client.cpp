#include "client.h"
#include "Utils/CConversions.h"

namespace edv {
namespace net {
namespace tcp {

client::client(const std::string& host, socket::port_t port) :
	addrinfo_(nullptr),
	host_(host),
	socket_(socket::invalid()),
	port_(port)
{
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family		= AF_INET;
    hints.ai_socktype	= SOCK_STREAM;

    if (getaddrinfo(host.c_str(), to_string(port_).c_str(), &hints, &addrinfo_) == 0)
    {
    	//@TODO examinar toda la respuesta de getaddrinfo y buscar familia y protocolo
    	socket_ = ::socket(addrinfo_->ai_family, addrinfo_->ai_socktype, addrinfo_->ai_protocol);
    	if (socket_ != socket::invalid())
    		connect(socket_, addrinfo_->ai_addr, addrinfo_->ai_addrlen);
    }

    //@TODO: check getaddrinfo(mismo valor retorno), connect(EDVSOCKETERROR) errors
}

client::~client()
{
//@TODO: este ifdef es temporal
#ifdef EDV_LINUX
	if (addrinfo_)
		free(addrinfo_);
#endif
}

const std::string& client::host() const
{
	return host_;
}

u16 client::port() const
{
	return port_;
}

}
}
}
