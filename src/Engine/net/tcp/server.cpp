#include "Core/CCoreEngine.h"
#include "server.h"
#include <cstring>
#include <chrono>
#include <algorithm>
#include <utility>
#include "Utils/CConversions.h"
#include "net/manager.h"
#include "Core/exceptions.h"

//#todo: "handshake" intercambiando valor de timeout y posiblemente otras cosas

namespace edv {
namespace net {
namespace tcp {

server::server(socket::port_t port, msg_handler* handler) :
	handler_(handler),
	addrinfo_(nullptr),
	port_(port),
	socket_(socket::invalid()),
	workers_(),
	running_(false),
	thread_()
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family		= AF_INET;
	hints.ai_socktype	= SOCK_STREAM;
	hints.ai_flags		= AI_PASSIVE;

	if (getaddrinfo(NULL, to_string(port).c_str(), &hints, &addrinfo_) == 0)
	{
		//#todo: procesar toda la lista que retorna getaddrinfo
		socket_ = ::socket(addrinfo_->ai_family, addrinfo_->ai_socktype, addrinfo_->ai_protocol);
		if (socket_ != socket::invalid())
		{
			if (socket::set_reuse(socket_))
			{
				if (bind(socket_, addrinfo_->ai_addr, addrinfo_->ai_addrlen) != EDVSOCKETERROR)
				{
					if (listen(socket_, 0) != EDVSOCKETERROR)
					{
						u32 num_threads = CCoreEngine::Instance().GetNetworkManager().srv_option(THREADS);
						for (u32 cnt = 0; cnt < num_threads; cnt++)
							workers_.emplace_back(new srv_worker(handler));

						socket::set_non_blocking(socket_);
						running_ = true;
						thread_ = std::thread(update, this);
						return;
					}
				}
			}
		}
	}

	throw core::exc::fatal_error("error creating " + description() + ": " + strerror(socket::last_error()));
}

server::~server()
{
	running_ = false;

	if (thread_.joinable())
		thread_.join();

	socket::close_socket(socket_);

//@TODO: este ifdef es temporal
#ifdef EDV_LINUX
	if (addrinfo_)
		free(addrinfo_);
#endif
}

bool server::running() const
{
	return running_;
}

socket::port_t server::port() const
{
	return port_;
}

void server::update(server* srv)
{
	const s32 accept_timeout = CCoreEngine::Instance().GetNetworkManager().srv_option(ACCEPT_TIMEOUT);

	while (srv->running())
	{
		srv->update_internal();
		std::this_thread::sleep_for(std::chrono::milliseconds(accept_timeout));
	}
}

void server::update_internal()
{
	sockaddr_storage cli_addr;
	socklen_t addr_size = sizeof(cli_addr);
	socket::socket_t cli_socket = accept(socket_, (sockaddr*)&cli_addr, &addr_size);

	if (cli_socket != EDVSOCKETERROR)
	{
		add_connection(new srv_connection(cli_socket, cli_addr));
	}
	else
	{
		s32 error = socket::last_error();

		if (error != EDVEWOULDBLOCK)
			LOG(LOG_ERROR, LOGSUB_NETWORK, "%s: accept error: %s", description().c_str(), strerror(error));
	}
}

std::string	server::description() const
{
	return std::string("tcp server on port " + to_string(port_));
}

void server::add_connection(srv_connection* connection)
{
	const s32 next = next_worker();
	if (next != -1)
		workers_[next]->insert(srv_worker::conn_ptr(connection));
}

s32 server::next_worker()
{
	s32 next			= -1;
	u32 min_connections	= std::numeric_limits<u32>::max();

	for (u32 idx = 0; idx < workers_.size(); idx++)
	{
		const u32 thread_sz = workers_[idx]->size();
		if (thread_sz < min_connections)
		{
			min_connections	= thread_sz;
			next			= idx;
		}
	}

	return next;
}

}
}
}
