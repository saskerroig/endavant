#ifndef TCPSERVER_H_
#define TCPSERVER_H_

#include <cstddef>
#include <vector>
#include <thread>
#include <atomic>
#include <memory>
#include "Core/CBasicTypes.h"
#include "net/socket.h"
#include "net/msg_handler.h"
#include "net/tcp/srv_connection.h"
#include "net/tcp/srv_worker.h"

namespace edv {
namespace net {
namespace tcp {

class server
{
public:
	server(socket::port_t port, msg_handler* handler);
	virtual ~server();

	bool			running() const;
	socket::port_t	port()	const;

private:
	typedef	std::unique_ptr<srv_worker>	worker_ptr;
	typedef std::vector<worker_ptr>		workers_t;

	static void		update(server* srv);
	void			update_internal();
	std::string		description() const;
	void			add_connection(srv_connection* connection);
	s32				next_worker();

	msg_handler*		handler_;
	addrinfo*			addrinfo_;
	socket::port_t		port_;
	socket::socket_t	socket_;
	workers_t			workers_;
	std::atomic<bool>	running_;
	std::thread			thread_;
};

}
}
}
#endif /* TCPSERVER_H_ */
