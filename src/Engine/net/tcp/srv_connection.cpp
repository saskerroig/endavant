#include "Core/CCoreEngine.h"
#include "srv_connection.h"
#include <ctime>
#include <utility>
#include <chrono>

using namespace std;

namespace edv {
namespace net {
namespace tcp {

srv_connection::srv_connection(socket::socket_t sck, const sockaddr_storage& addr) :
	socket_(sck),
	cli_timeout_(CCoreEngine::Instance().GetNetworkManager().srv_option(CLIENT_TIMEOUT) / 1000)
{
    char address[INET_ADDRSTRLEN];
    sockaddr_in* saddr = (sockaddr_in*)&addr;

	inet_ntop(AF_INET, &saddr->sin_addr, address, sizeof(address));
    ip_ 	= address;
    port_	= ntohs(saddr->sin_port);

	time(&timeout_check_);
}

srv_connection::~srv_connection()
{
	socket::close_socket(socket_);
}

socket::socket_t srv_connection::socket() const
{
	return socket_;
}

const std::string& srv_connection::ip() const
{
	return ip_;
}

socket::port_t srv_connection::port() const
{
	return port_;
}

bool srv_connection::timeout() const
{
	time_t now;
	time(&now);

	if (now - timeout_check_ >= cli_timeout_)
		return true;

	return false;
}

void srv_connection::update()
{
	time(&timeout_check_);
}

}
}
}
