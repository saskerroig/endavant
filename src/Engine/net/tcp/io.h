#ifndef EDV_NET_IO_H_
#define EDV_NET_IO_H_

#include "Core/CBasicTypes.h"
#include "Core/CCoreEngine.h"
#include "net/buffer.h"
#include "net/socket.h"

namespace edv	{
namespace net	{
namespace tcp	{

class io
{
public:
	template <size_type Capacity>
		static bool send(socket::socket_t sck, const buffer<Capacity>& buffer);
	template <size_type Capacity>
		static bool receive(socket::socket_t sck, buffer<Capacity>* buffer);
	template <size_type Capacity>
		static bool receive_nb(socket::socket_t sck, buffer<Capacity>* buffer);

private:

	static bool send_raw(socket::socket_t sck, const char* orig, size_type size); // send en windows recibe char*, linux void*
	static bool receive_raw(socket::socket_t sck, char* dst, size_type size); // receive en windows recibe char*, linux void*
};

template <size_type Capacity>
bool io::send(socket::socket_t sck, const buffer<Capacity>& buffer)
{
	const size_type buffer_size = buffer.size();

	return	send_raw(sck, reinterpret_cast<const char*>(&buffer_size), sizeof(buffer_size)) &&
			send_raw(sck, reinterpret_cast<const char*>(buffer.data()), buffer_size);
}

template <size_type Capacity>
bool io::receive(socket::socket_t sck, buffer<Capacity>* buffer)
{
	size_type buffer_size = 0;

	if (receive_raw(sck, reinterpret_cast<char*>(&buffer_size), sizeof(buffer_size)))
	{
		if (!buffer->fits(buffer_size))
		{
			if (!buffer->resize(buffer_size))
				return false; //@TODO: excepcion
		}

		if (receive_raw(sck, reinterpret_cast<char*>(buffer->data()), buffer_size))
		{
			buffer->rewind();
			return true;
		}
	}

	return false;
}

template <size_type Capacity>
  bool io::receive_nb(socket::socket_t sck, buffer<Capacity>* buffer)
{
	size_type length = 0;
	size_type len_sz = sizeof(length);

	socket::set_non_blocking(sck);

	if (socket::waiting_bytes(sck) >= len_sz)
	{
		s32 ret = recv(sck, reinterpret_cast<char*>(&length), len_sz, MSG_PEEK);
		if (ret == len_sz)
		{
			if (socket::waiting_bytes(sck) >= len_sz + length)
			{
				if (!buffer->fits(length))
				{
					if (!buffer->resize(length))
						return false;//@TODO: esto deberia ser una excepcion
				}

				if (receive_raw(sck, reinterpret_cast<char*>(&length), len_sz))
				{
					if (receive_raw(sck, reinterpret_cast<char*>(buffer->data()), length))
					{
						buffer->rewind();
						return true;
					}
					//else aqui la liamos, tambien excepcion
				}
			}
		}
		else if (ret == EDVSOCKETERROR && socket::last_error() != EDVEWOULDBLOCK)
	    	LOG(LOG_ERROR, LOGSUB_NETWORK, "recv: %s", strerror(socket::last_error()));
	}

	return false;
}

}
}
}
#endif
