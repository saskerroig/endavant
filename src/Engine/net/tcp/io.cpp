#include "io.h"

namespace edv {
namespace net {
namespace tcp {

// por esto: http://www.beej.us/guide/bgnet/output/html/multipage/advanced.html#sendall
bool io::send_raw(socket::socket_t sck, const char* orig, size_type size)
{
	socket::set_blocking(sck);

    s32 sent = 0;
    s32 left = size;

    do
    {
		s32 result = ::send(sck, orig + sent, left, 0);
		if (result == EDVSOCKETERROR)
		{
			LOG(LOG_ERROR, LOGSUB_NETWORK, "send: %s", strerror(socket::last_error()));
			return false;
		}

		sent += result;
		left -= result;
    }
    while(sent < (s32)size);

//	LOG(LOG_DEVEL, LOGSUB_NETWORK, "sent %d bytes", sent);

    return true;
}

bool io::receive_raw(socket::socket_t sck, char* dst, size_type size) // receive en windows recibe char*, linux void*
{
	socket::set_blocking(sck);

    s32 ret = recv(sck, dst, size, 0);

    if (ret == -1)
    {
    	LOG(LOG_ERROR, LOGSUB_NETWORK, "recv: %s", strerror(socket::last_error()));
		return false;
    }
    else if (ret == 0)
    	return false;

//	LOG(LOG_DEVEL, LOGSUB_NETWORK, "recv %d bytes", ret);

    return true;
}

}
}
}
