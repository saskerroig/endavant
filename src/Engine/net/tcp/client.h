#ifndef EDV_NET_TCP_CLIENT_H_
#define EDV_NET_TCP_CLIENT_H_

#include <string>
#include "Core/CBasicTypes.h"
#include "net/socket.h"
#include "net/tcp/io.h"

namespace edv {
namespace net {
namespace tcp {

class client
{
public:
	client(const std::string& host, socket::port_t port);
	virtual ~client();

	const std::string&	host() const;
	u16 				port() const;

	template <size_type Capacity>
		bool send(const buffer<Capacity>& out);
	template <size_type Capacity>
		bool receive(buffer<Capacity>* in);
	template <size_type Capacity>
		bool receive_nb(buffer<Capacity>* in);

private:
	addrinfo* 			addrinfo_;
	std::string			host_;
	socket::socket_t	socket_;
	socket::port_t		port_;
};

template <size_type Capacity>
	bool client::send(const buffer<Capacity>& out)
{
	return io::send(socket_, out);
}

template <size_type Capacity>
	bool client::receive(buffer<Capacity>* in)
{
	return io::receive(socket_, in);
}

template <size_type Capacity>
	bool client::receive_nb(buffer<Capacity>* in)
{
	return io::receive_nb(socket_, in);
}

}
}
}
#endif
