#ifndef EDV_NET_SRV_WORKER_H_
#define EDV_NET_SRV_WORKER_H_

#include <list>
#include <memory>
#include <thread>
#include <mutex>
#include <atomic>
#include "net/msg_handler.h"
#include "net/tcp/io.h"
#include "net/tcp/srv_connection.h"

namespace edv {
namespace net {
namespace tcp {

class srv_worker
{
public:
	typedef std::shared_ptr<srv_connection>	conn_ptr;
	typedef std::list<conn_ptr>				connections_t;

	srv_worker(msg_handler* handler);
	virtual ~srv_worker();

	void			insert(const conn_ptr& connection);
	u32				size();

private:
	static void read_msgs(srv_worker* worker);
	void read_msgs_internal();

	msg_handler*		handler_;
	connections_t		connections_;
	buffer<> 			packet_in_;
	buffer<>			packet_out_;
	std::atomic<bool>	active_;
	std::mutex			mutex_;
	std::thread			thread_;
};

}
}
}
#endif
