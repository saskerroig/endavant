#include "net/tcp/srv_worker.h"
#include "Core/CCoreEngine.h"

namespace edv {
namespace net {
namespace tcp {

srv_worker::srv_worker(msg_handler* handler) :
	handler_(handler),
	connections_(),
	active_(true),
	mutex_(),
	thread_(read_msgs, this)
{
	LOG(LOG_DEVEL, LOGSUB_NETWORK, "creating worker thread %s", to_string(thread_.get_id()).c_str());
}

srv_worker::~srv_worker()
{
	active_ = false;

	if (thread_.joinable())
		thread_.join();
}

void srv_worker::insert(const conn_ptr& connection)
{
	std::lock_guard<std::mutex> lock(mutex_);

	connections_.push_back(conn_ptr(connection));
	LOG(LOG_DEVEL, LOGSUB_NETWORK, "connection %s:%u added in worker %s (size:%d)", connection->ip().c_str(), connection->port(), to_string(thread_.get_id()).c_str(), connections_.size());
}

u32 srv_worker::size()
{
	std::lock_guard<std::mutex> lock(mutex_);
	return connections_.size();
}

void srv_worker::read_msgs(srv_worker* worker)
{
	while(worker->active_)
	{
		worker->read_msgs_internal();
		std::this_thread::sleep_for(std::chrono::milliseconds(5));//#todo: configurable
	}
}

void srv_worker::read_msgs_internal()
{
	std::lock_guard<std::mutex> lock(mutex_);
	auto it = connections_.begin();

	while(it != connections_.end())
	{
		srv_connection* connection = it->get();

		if (io::receive_nb(connection->socket(), &packet_in_))
		{
			connection->update();

			packet_in_.rewind();
			packet_out_.reset();
			handler_->process_message(packet_in_, &packet_out_);

			io::send(connection->socket(), packet_out_);
		}

		if (connection->timeout())
		{
			//#todo: se debe poder informar de las desconexiones a los usuarios de la clase tcp::server
			//#todo: nivel de "verbosidad" de logs
			LOG(LOG_INFO, LOGSUB_NETWORK, "connection from %s:%u closed. %u active connections on worker %s",
					connection->ip().c_str(), connection->port(), connections_.size() - 1,
					to_string(thread_.get_id()).c_str());
			it = connections_.erase(it);
		}
		else
			it++;
	}
}

}
}
}
