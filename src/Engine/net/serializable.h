/*
 * serializable.h
 *
 *  Created on: 07/07/2013
 *      Author: edu
 */

#ifndef SERIALIZABLE_H_
#define SERIALIZABLE_H_

#include "Core/CBasicTypes.h"
#include "net/buffer.h"

namespace edv {
namespace net {

class serializable
{
public:
	serializable() {}
	virtual ~serializable() {}

	virtual s32 	serialized_size() const = 0; // en realidad es solo necesaria con buffers estaticos
	virtual void 	serialize(buffer<>* dst) const = 0;
	virtual void 	deserialize(buffer<>& orig) = 0;
};

}
}
#endif /* SERIALIZABLE_H_ */
