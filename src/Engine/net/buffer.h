#ifndef BUFFER_H_
#define BUFFER_H_

#include <cstddef>
#include <cstring>
#include <utility>
#include <memory>
#include "Core/CBasicTypes.h"
#include "net/types.h"

#define DYNAMIC_SIZE	0

namespace edv {
namespace net {

template <net::size_type Capacity = DYNAMIC_SIZE>
class buffer
{
public:
	buffer(size_type capacity = Capacity);
	template <typename... Args> buffer(Args&&... args);
	template <size_type Rcapacity> buffer(const buffer<Rcapacity>& other);
	~buffer();

	void	rewind();
	void 	reset();
	void	clear();
  template <typename T>
	bool 	write(const T& orig, size_type size = sizeof(T));
    bool	write(const std::string& str);
  template <typename T, typename... Args>
  	bool 	write_n(T&& first, Args&&... arg);
  template <typename T>
	bool 	read(T* dst, size_type size = sizeof(T));
    bool	read(std::string* dst);
  template <typename T, typename... Args>
	bool	read_n(T* first, Args&&... args);
	size_type 	size() const;
	size_type 	capacity() const;
	u8* 	data();
	const u8* data() const;
  template <size_type Rcapacity>
	buffer& operator=(const buffer<Rcapacity>& other);
	bool 	fits(size_type size) const;
	bool	resize(size_type capacity);

private:
	void init(size_type capacity);
	void destroy();
	void grow(size_type hint);
	bool dynamic() const;
    bool write_n();
    bool read_n();

	size_type 	index_;
	size_type 	size_;
	size_type 	capacity_;
	u8*			data_;
	u8 			data_static_[Capacity];
};

template <size_type Capacity>
buffer<Capacity>::buffer(size_type capacity) :
	size_(0),
	capacity_(Capacity),
	data_(nullptr)
{
	init(capacity);
}

template <size_type Capacity>
template <typename... Args>
buffer<Capacity>::buffer(Args&&... args) :
	size_(0),
	capacity_(Capacity),
	data_(nullptr)
{
	init(capacity_);
	write_n(args...);
}

template <size_type Capacity>
template <size_type Rcapacity>
buffer<Capacity>::buffer(const buffer<Rcapacity>& other)
{
	*this = other;
}

template <size_type Capacity>
buffer<Capacity>::~buffer()
{
	destroy();
}

template <size_type Capacity>
void buffer<Capacity>::rewind()
{
	index_ = 0;
}

template <size_type Capacity>
void buffer<Capacity>::reset()
{
	rewind();
	size_ = 0;
}

template <size_type Capacity>
void buffer<Capacity>::clear()
{
	if (dynamic())
		capacity_ = 0;

	init(capacity_);
}

template <size_type Capacity>
size_type buffer<Capacity>::size() const
{
	return size_;
}

template <size_type Capacity>
size_type buffer<Capacity>::capacity() const
{
	return capacity_;
}

template <size_type Capacity>
template <typename T>
bool buffer<Capacity>::write(const T& orig, size_type size)
{
	if (dynamic() && !fits(size))
	{
		grow(size);
	}
		
	if (fits(size))
	{
		memcpy(data_ + index_, &orig, size);
		index_ += size;

		if (index_ > size_)
			size_ += size;

		return true;
	}

	return false;
}

template <size_type Capacity>
  bool buffer<Capacity>::write(const std::string& str)
{
	const u16 str_sz = str.size();	// max string size: 65535 chars
	return
			write(str_sz)				&&
			write(*str.c_str(), str_sz);
}

template <size_type Capacity>
  bool buffer<Capacity>::write_n()
{
	return true;
}

template <size_type Capacity>
template <typename T, typename... Args>
  bool 	buffer<Capacity>::write_n(T&& first, Args&&... args)
{
	return
			write(first)	&&
			write_n(std::forward<Args>(args)...);
}

template <size_type Capacity>
template <typename T>
bool buffer<Capacity>::read(T* dst, size_type size)
{
	if (fits(size))
	{
		memcpy(dst, data_ + index_, size);
		index_ += size;
		return true;
	}

	return false;
}

template <size_type Capacity>
bool buffer<Capacity>::read(std::string* dst)
{
	u16 str_sz = 0;
	if (read(&str_sz))
	{
		std::unique_ptr<char[]> b(new char[str_sz + 1/*\0*/]);
		if (read(b.get(), str_sz))
		{
			b.get()[str_sz] = 0;
			*dst = b.get();
			return true;
		}
	}

	return false;
}

template <size_type Capacity>
  bool buffer<Capacity>::read_n()
{
	return true;
}

template <size_type Capacity>
template <typename T, typename... Args>
  bool	buffer<Capacity>::read_n(T* first, Args&&... args)
{
	return
			read(first)		&&
			read_n(std::forward<Args>(args)...);
}

template <size_type Capacity>
u8* buffer<Capacity>::data()
{
	return data_;
}

template <size_type Capacity>
const u8* buffer<Capacity>::data() const
{
	return data_;
}

template <size_type Capacity>
template <size_type Rcapacity>
buffer<Capacity>& buffer<Capacity>::operator=(const buffer<Rcapacity>& other)
{
	if (dynamic())
	{
		destroy();
		index_ = other.index_;
		size_ = other.size();
		capacity_ = other.capacity();
		data_ = new u8[capacity_];
		memcpy(data_, other.data(), capacity_);
	}
	else
	{
		if (Capacity <= other.capacity())
		{
			index_ = other.index_;
			size_ = other.size();
			memcpy(data_, other.data(), size_);
		}
	}

	return *this;
}

template <size_type Capacity>
void buffer<Capacity>::init(size_type capacity)
{
	reset();

	if (dynamic())
	{
		destroy();
		capacity_ = capacity;
		if (!capacity_)
			capacity_ = sizeof(u32);
		data_ = new u8[capacity_];
	}
	else
	{
		data_ = &data_static_[0];
	}
}

template <size_type Capacity>
void buffer<Capacity>::destroy()
{
	if (dynamic() && data_)
	{
		delete[] data_;
		data_ = nullptr;
	}
}

template <size_type Capacity>
void buffer<Capacity>::grow(size_type hint)
{
	if (dynamic())
	{
		if (capacity_ * 2 > hint)
			capacity_ *= 2;
		else
			capacity_ = hint * 2;

		u8* data = new u8[capacity_];
		memcpy(data, data_, size_);
		destroy();
		data_ = data;
	}
}

template <size_type Capacity>
bool buffer<Capacity>::dynamic() const
{
	return Capacity == DYNAMIC_SIZE;
}

template <size_type Capacity>
bool buffer<Capacity>::fits(size_type size) const
{
	return index_ + size <= capacity_;
}

template <size_type Capacity>
bool buffer<Capacity>::resize(size_type capacity)
{
	if (dynamic())
	{
		if (capacity != capacity_)
		{
			u8* data = new u8[capacity];
			
			if (capacity < capacity_)
			{
				if (size_ > capacity)
					size_ = capacity;
				if (index_ > capacity)
					index_ = capacity;

			}

			memcpy(data, data_, size_);
			destroy();
			data_ = data;
			capacity_ = capacity;
		}

		return true;
	}

	return false;
}

}
}
#endif
