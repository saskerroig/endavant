#ifndef EDV_NET_SOCKET_H_
#define EDV_NET_SOCKET_H_

#include "Core/Defines.h"
#include "Core/CBasicTypes.h"

#ifdef EDV_LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#define EDVEWOULDBLOCK	EWOULDBLOCK
#define EDVINVALIDSOCK	0xFF		// windows INVALID_SOCKET
#define EDVSOCKETERROR	-1			// return value of send, recv, ...

#elif defined(EDV_WINDOWS)
#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x1000 // guarrada para que coja getaddrinfo de ws2tcpip.h, ya examinare las consecuencias xDD
#endif
#include <winsock2.h>
#include <ws2tcpip.h>

#define EDVEWOULDBLOCK	WSAEWOULDBLOCK
#define EDVINVALIDSOCK	INVALID_SOCKET
//static_assert(INVALID_SOCKET == 0xFF, "Check EDVINVALIDSOCK compatibility");
#define EDVSOCKETERROR	SOCKET_ERROR

#endif

namespace edv		{
namespace net		{
namespace socket	{

#ifdef EDV_LINUX
typedef	s32		socket_t;
#elif defined(EDV_WINDOWS)
typedef SOCKET	socket_t;
#endif

typedef	u16		port_t;

bool		init();
void		stop();
socket_t	invalid();
void		close_socket(socket_t sck);
void 		set_blocking(socket_t sck);
void		set_non_blocking(socket_t sck);
bool		set_reuse(socket_t sck);
int			last_error();
s32			waiting_bytes(socket_t sck);

}
}
}

#endif
