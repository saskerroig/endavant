#ifndef MANAGER_H_
#define MANAGER_H_

#include <map>
#include <mutex>
#include "Core/ISubSystem.h"
#include "Utils/xml_enum.h"

namespace edv
{
namespace net
{

enum NET_SRV_OPTION
{
	NET_SRV_OPT_NONE = 0,
	ACCEPT_TIMEOUT,
	CLIENT_TIMEOUT,
	THREADS,
	NET_SRV_OPT_ALL
};

class manager : public ISubSystem
{
public:
	manager();
	virtual ~manager();

	void StartUp();
	void ShutDown();
	void Update(f64 dt);

	s32 srv_option(NET_SRV_OPTION option);

private:
	typedef std::map<NET_SRV_OPTION, s32>		srv_options_t;

	void load_options();

	util::xml_enum<NET_SRV_OPTION, NET_SRV_OPT_ALL> 	srv_options_ids_;
	srv_options_t										srv_options_;
	std::mutex											srv_opts_mutex_;
};

} /* namespace net */
} /* namespace edv */
#endif /* MANAGER_H_ */
