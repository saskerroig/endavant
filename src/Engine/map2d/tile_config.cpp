#include "map2d/tile_config.h"

namespace edv	{
namespace map2d	{

tile_config::tile_config() :
	obstacle_(false),
	character_(false),
	frames_(0),
	speed_(0),
	image_(""),
	tile_width_(0),
	tile_height_(0)
{

}

tile_config::tile_config(bool obstacle, bool character, u32 frames, f32 speed, const std::string& image, u32 tile_width, u32 tile_height) :
	obstacle_(obstacle),
	character_(character),
	frames_(frames),
	speed_(speed),
	image_(image),
	tile_width_(tile_width),
	tile_height_(tile_height)
{

}

}
}
