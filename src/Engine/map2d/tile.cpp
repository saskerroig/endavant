#include "map2d/tile.h"

namespace edv	{
namespace map2d	{

void tile::add_object(map_object* object)
{
	objects_.push_front(object);
}

void tile::remove_object(map_object* object)
{
	for (auto it = objects_.begin(); it != objects_.end(); it++)
	{
		if (*it == object)
		{
			it = objects_.erase(it);
			break;
		}
	}
}

bool tile::empty() const
{
	return objects_.empty();
}

bool tile::obstacle() const
{
	return objects_.empty()? true : objects_.front()->obstacle();
}

}
}
