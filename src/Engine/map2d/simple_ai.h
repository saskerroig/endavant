#ifndef EDV_MAP2D_SIMPLE_IA_H_
#define EDV_MAP2D_SIMPLE_IA_H_

#include <cstdlib>
#include <vector>
#include <array>
#include <algorithm>
#include "Core/CBasicTypes.h"
#include "map2d/tile_map.h"

namespace edv	{
namespace map2d	{

class simple_ai
{
public:
	simple_ai(s32 id, f32 amount, f32 time);
	void move(tile_map& map);

private:
	s32							id_;
	f32							amount_;
	f32							time_;
	std::array<glm::vec2, 4>	directions_;
	glm::vec2					direction_;
};

}
}

#endif
