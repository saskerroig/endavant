#include "map2d/tile_map.h"
#include <algorithm>
#include "Utils/math.h"

const std::string XML_CONFIG("Config/tile_map_options.xml");

namespace edv	{
namespace map2d	{

//#todo logs en propio "subsistema"

tile_map::tile_map(const std::string& tileset, const std::string& path) :
	rows_(0),
	cols_(0),
	grid_(nullptr),
	curr_character_id_(0),
	curr_character_z(1),
	root_(new CBaseNode)
{
	load_tileset(tileset);
	load_xml(path);

	root_->Show();
}

tile_map::~tile_map()
{
	destroy_grid();
}

void tile_map::load_tileset(const std::string& tileset)
{
	CXMLParserPUGI parser(XML_CONFIG);

	if (parser.Ready())
	{
		std::string node("TILE_MAP_OPTIONS/TILESETS/TILESET");
		u32 tilesets = parser.GetNodeNameCount(node);
		for (u32 tset = 0; tset < tilesets; tset++)
		{
			const std::string current_tileset(node + "#" + to_string(tset));
			const std::string tileset_name(parser.GetStringAttributeValue(current_tileset, "name"));
			if (tileset_name == tileset)
			{
				const s32 tile_width			= parser.GetIntAttributeValue(current_tileset, "tile_width_px", 32);
				const s32 tile_height			= parser.GetIntAttributeValue(current_tileset, "tile_height_px", 32);

				const std::string default_tile	= parser.GetStringAttributeValue(current_tileset, "default_tile", " ");
				if (default_tile.size() != 1)
				{
					LOG(LOG_ERROR, LOGSUB_ENGINE, "Error reading default tile for tileset '%s'", tileset.c_str());
					continue;
				}

				node = current_tileset + "/TILE";
				u32 tiles = parser.GetNodeNameCount(node);

				for (u32 tile = 0; tile < tiles; tile++)
				{
					const std::string current_tile(node + "#" + to_string(tile));
					const std::string id(parser.GetStringAttributeValue(current_tile, "id"));

					if (id.size() != 1)
					{
						LOG(LOG_ERROR, LOGSUB_ENGINE, "Error reading id of tile in tileset '%s'", tileset.c_str());
						return;
					}

					auto it = object_lib_.find(id[0]);
					if (it == object_lib_.end())
					{
						tile_config config(	parser.GetBoolAttributeValue(current_tile, "obstacle", false),
											parser.GetBoolAttributeValue(current_tile, "character", false),
											parser.GetIntAttributeValue(current_tile, "frames", 1),
											parser.GetFloatAttributeValue(current_tile, "speed", 0.1),
											parser.GetStringAttributeValue(current_tile, "image"),
											tile_width,
											tile_height);

						if (id[0] == default_tile[0])
							default_tile_ = config;

						LOG(LOG_DEVEL, LOGSUB_ENGINE, "Loaded configuration for id '%c' (obstacle:%d character:%d frames:%d speed:%.3f image:%s",
								id[0],
								config.obstacle_,
								config.character_,
								config.frames_,
								config.speed_,
								config.image_.c_str());

						object_lib_.insert(it, object_lib_t::value_type(id[0], config));
					}
				}

				if (default_tile_.image_.empty())
				{
					LOG(LOG_ERROR, LOGSUB_ENGINE, "Default tile not specified for tileset '%s'", tileset.c_str());
					return;
				}

				break; // hemos encontrado el tileset que buscabamos
			}
		}

		node = "TILE_MAP_OPTIONS/CHARACTERS/CHARACTER";
		u32 characters = parser.GetNodeNameCount(node);

		for (u32 character = 0; character < characters; character++)
		{
			const std::string current_char(node + "#" + to_string(character));
			const std::string char_name(parser.GetStringAttributeValue(current_char, "name"));

			auto it = character_lib_.find(char_name);
			if (it != character_lib_.end())
			{
				LOG(LOG_WARNING, LOGSUB_ENGINE, "character with name '%s' already exists, skipping", char_name.c_str());
				continue;
			}

			node = current_char + "/ACTION";
			u32 actions = parser.GetNodeNameCount(node);

			animations_t animations;
			for (u32 action = 0; action < actions; action++)
			{
				const std::string current_action(node + "#" + to_string(action));
				const std::string action_name(parser.GetStringAttributeValue(current_action, "name"));
				const std::string tile_id(parser.GetStringAttributeValue(current_action, "tile"));

				if (tile_id.size() != 1)
				{
					LOG(LOG_ERROR, LOGSUB_ENGINE, "Error reading tile id for action '%s' of character '%s'", action_name.c_str(), char_name.c_str());
					continue;
				}

				auto it = object_lib_.find(tile_id[0]);
				if (it != object_lib_.end())
				{
					const tile_config& config = it->second;

					auto it_anim = animations.find(action_name);
					if (it_anim == animations.end())
					{
						animations.insert(it_anim, animations_t::value_type(action_name, config));
						LOG(LOG_DEVEL, LOGSUB_ENGINE, "Loaded action '%s' with tile id '%c' for character '%s'",
								action_name.c_str(), tile_id[0], char_name.c_str());
					}
					else
						LOG(LOG_ERROR, LOGSUB_ENGINE, "Duplicated action '%s' for character '%s'", action_name.c_str(), char_name.c_str());
				}
			}

			LOG(LOG_DEVEL, LOGSUB_ENGINE, "%d actions associated with character '%s'", animations.size(), char_name.c_str());
			character_lib_.insert(it, character_lib_t::value_type(char_name, animations));
		}
	}
}

bool tile_map::load_xml(const std::string& path)
{
	start_positions_.clear();
	CXMLParserPUGI parser(path);

	if (parser.Ready())
	{
		std::string node("BLOCK_MAP/ROW");
		rows_ = parser.GetNodeNameCount(node);

		if (rows_)
		{
			cols_ = parser.GetStringAttributeValue(node + "#" + to_string(0), "value").size();

			if (cols_)
			{
				create_grid(rows_, cols_);

				for (u32 row = 0; row < rows_; row++)
				{
					std::string row_str = parser.GetStringAttributeValue(node + "#" + to_string(row), "value");

					if (row_str.size() == cols_)
					{
						for (u32 col = 0; col < cols_; col++)
						{
							if (grid_[row][col].empty())
							{
								auto it = object_lib_.find(row_str[col]);

								if (it != object_lib_.end())
								{
									tile_config config(it->second);

									if (config.character_)
									{
										start_positions_.push_back(position_t(row, col));
										config = default_tile_;
									}

									object_ptr obj_ptr(new map_object(config));

									if (!put_in_grid(obj_ptr.get(), row, col))
									{
										LOG(LOG_ERROR, LOGSUB_ENGINE, "No enough space for id '%c' in row:%d col:%d", row_str[col], row, col);
										return false;
									}

									LOG(LOG_DEVEL, LOGSUB_ENGINE, "w:%d, h:%d, rows:%d, cols:%d, tile_size:%dx%d ponemos objeto con id:%c en grid[%d][%d] con pos %f,%f",
											obj_ptr.get()->width(),
											obj_ptr.get()->height(),
											rows_,
											cols_,
											config.tile_width_,
											config.tile_height_,
											row_str[col],
											row,
											col,
											obj_ptr.get()->node()->GetPosition().x,
											obj_ptr.get()->node()->GetPosition().y);

									active_objects_.emplace_back(std::move(obj_ptr));
								}
								else
								{
									LOG(LOG_ERROR, LOGSUB_ENGINE, "tile id '%c' not present in loaded tileset", row_str[col]);
									return false;
								}
							}
						}
					}
					else
					{
						LOG(LOG_ERROR, LOGSUB_ENGINE, "Different column sizes reading %s", path.c_str());
						return false;
					}
				}

				return true;
			}
		}
	}

	return false;
}

void tile_map::create_grid(u32 rows, u32 cols)
{
	destroy_grid();

	grid_ = new tile*[rows];
	for (u32 row = 0; row < rows; row++)
		grid_[row] = new tile[cols];
}

void tile_map::destroy_grid()
{
	if (grid_)
	{
		for (u32 row = 0; row < rows_; row++)
			delete[] grid_[row];

		delete[] grid_;
	}
}

s32 tile_map::add_character(const std::string& name)
{
	s32 id = -1;

	auto it = character_lib_.find(name);
	if (it != character_lib_.end())
	{
		const animations_t& animations = it->second;

		auto it_default = animations.find("default");
		if (it_default != animations.end())
		{
			character_ptr character(new map_character(it_default->second));

//#todo cascar antes si no hay posiciones iniciales
			std::random_shuffle(start_positions_.begin(), start_positions_.end());

			if (put_in_grid(character.get(), start_positions_.front().row, start_positions_.front().col, true))
			{
				for (auto it_anim = animations.begin(); it_anim != animations.end(); it_anim++)
					character->add_action(it_anim->first, it_anim->second);

				id = curr_character_id_++;
				active_characters_[id] = std::move(character);

				LOG(LOG_DEVEL, LOGSUB_ENGINE, "Character '%s' added with id %d", name.c_str(), id);
			}
		}
	}

	return id;
}

bool tile_map::remove_character(s32 id)
{
	auto it = active_characters_.find(id);

	if (it != active_characters_.end())
	{
		active_characters_.erase(it);
		LOG(LOG_DEVEL, LOGSUB_ENGINE, "Character with id %d removed", id);
		return true;
	}

	return false;
}

bool tile_map::can_move_character(s32 id, const glm::vec2& amount) const
{
	auto it = active_characters_.find(id);

	if (it != active_characters_.end())
	{
		map_character* character = it->second.get();
		const glm::vec2 origin(character->position().x, character->position().y);
		const glm::vec2 dest(origin.x + amount.x, origin.y + amount.y);

		return can_move(character, origin, dest);
	}

	return false;
}

bool tile_map::move_character(s32 id, const glm::vec2& amount, f32 time)
{
	auto it = active_characters_.find(id);

	if (it != active_characters_.end())
	{
		map_character* character = it->second.get();
		const glm::vec2 origin(character->position().x, character->position().y);
		const glm::vec2 dest(origin.x + amount.x, origin.y + amount.y);

		if (can_move(character, origin, dest))
		{
			position_t dst_pos		= to_grid_coord(dest);
			position_t origin_pos	= to_grid_coord(origin);

			for (u32 row = dst_pos.row; row < dst_pos.row + character->height(); row++)
				for (u32 col = dst_pos.col; col < dst_pos.col + character->width(); col++)
					grid_[row][col].add_object(character);

			for (u32 row = origin_pos.row; row < origin_pos.row + character->height(); row++)
				for (u32 col = origin_pos.col; col < origin_pos.col + character->width(); col++)
					grid_[row][col].remove_object(character);

			character->move(map_character::path_t(origin, dest, time));

			return true;
		}
	}

	return false;
}

//tile_map::characters_t tile_map::get_characters() const
//{
//	characters_t characters;
//
//	for (auto& pair : active_characters_)
//		characters.push_back(pair.second.get());
//
//	return characters;
//}

bool tile_map::put_in_grid(map_object* obj, u32 row, u32 col, bool character)
{
	if (row + obj->height() <= rows_ && col + obj->width() <= cols_)
	{
		root_->AddChild(obj->node());

		glm::vec3 pos(col * default_tile_.tile_width_, row * default_tile_.tile_height_, character?curr_character_z++:0);
		pos.y *= -1.0f;
		LOG(LOG_DEVEL, LOGSUB_ENGINE, "pos: %f %f %f", pos.x, pos.y, pos.z);

		obj->set_position(pos);
		obj->node()->Show();

		for (u32 y = row; y < row + obj->height(); y++)
			for (u32 x = col; x < col + obj->width(); x++)
				grid_[y][x].add_object(obj);

		return true;
	}

	return false;
}

bool tile_map::set_character_action(s32 id, const std::string& action_id)
{
	auto it = active_characters_.find(id);

	if (it != active_characters_.end())
	{
		it->second->do_action(action_id);
		return true;
	}

	return false;
}

bool tile_map::check_obstacle(map_object* object, f32 x, f32 y) const
{
	position_t position = to_grid_coord(glm::vec2(x, y));

	for (u32 h = position.row; h < position.row + object->height(); h++)
		for (u32 w = position.col; w < position.col + object->width(); w++)
			if (grid_[h][w].obstacle())
				return false;

	return true;
}

bool tile_map::can_move(map_object* obj, const glm::vec2& origin, const glm::vec2& dest) const
{
//#todo reducir los puntos a comprobar
//#todo otro personaje, enemigo...

	if (origin.x == dest.x)
	{
		for (f32 y = origin.y; origin.y>dest.y?y>=dest.y:y<=dest.y; origin.y>dest.y?y--:y++)
			if (!check_obstacle(obj, origin.x, y))
				return false;
	}
	else
	{
		for (f32 x = origin.x; origin.x>dest.x?x>=dest.x:x<=dest.x; origin.x>dest.x?x--:x++)
		{
			f32 y = origin.y + ((dest.y-origin.y)/(dest.x-origin.x))*(x-origin.x);

			if (!check_obstacle(obj, x, y))
				return false;
		}
	}

	return true;
}

tile_map::position_t tile_map::to_grid_coord(const glm::vec2& world_pos) const
{
	position_t position;

	position.row	= math::div_add_remainder(world_pos.y*-1, default_tile_.tile_height_);
	position.col	= math::div_add_remainder(world_pos.x, default_tile_.tile_width_);

	return position;
}

}
}

