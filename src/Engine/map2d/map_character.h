#ifndef EDV_MAP2D_MAP_CHARACTER_
#define EDV_MAP2D_MAP_CHARACTER_

#include <string>
#include <unordered_map>
#include "map2d/map_object.h"
#include "map2d/tile_config.h"

namespace edv	{
namespace map2d	{

class map_character : public map_object
{
public:
	struct path_t
	{
		glm::vec2	origin;
		glm::vec2	dest;
		f32			time;

		path_t() : origin(0,0), dest(0,0), time(0) { }
		path_t(const glm::vec2& o, const glm::vec2& d, f32 t) : origin(o), dest(d), time(t) { }
	};

	map_character(const tile_config& config);

	bool add_action(const std::string& id, const tile_config& config);
	bool do_action(const std::string& id);
	void move(const path_t& path);

private:
	typedef std::unordered_map<std::string, tile_config>	actions_t;
	typedef std::deque<path_t>								current_path_t;

	static void movement_finished(void* obj, void* caller);

	actions_t		actions_;
	current_path_t	current_path_;
};

}
}

#endif
