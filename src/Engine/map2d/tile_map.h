#ifndef EDV_MAP2D_TILE_MAP_
#define EDV_MAP2D_TILE_MAP_

#include <string>
#include <unordered_map>
#include "map2d/tile.h"
#include "map2d/tile_config.h"
#include "map2d/map_character.h"
#include "Renderer/Draw_Nodes/CSprite.h"
#include "Renderer/Draw_Nodes/CSpriteAnimated.h"

//#todo separar logica de pintado

namespace edv	{
namespace map2d	{

class tile_map
{
public:
	tile_map(const std::string& tileset, const std::string& path);
	virtual ~tile_map();

	CBaseNode*	root() { return root_.get(); }

	s32			add_character(const std::string& name);
	bool		remove_character(s32 id);

	bool		can_move_character(s32 id, const glm::vec2& amount) const;
	bool		move_character(s32 id, const glm::vec2& amount, f32 time);

//	bool		move_character(s32 id, s32 x_distance, s32 y_distance, f32 time);
	bool		set_character_action(s32 id, const std::string& action_id);

private:
	struct position_t
	{
		s32 row;
		s32 col;

		position_t() : row(0), col(0) { }
		position_t(s32 r, s32 c) : row(r), col(c) { }
	};

	typedef std::unique_ptr<CBaseNode>						node_ptr;
	typedef std::unique_ptr<map_object>						object_ptr;
	typedef std::unique_ptr<map_character>					character_ptr;
	typedef std::unordered_map<char, tile_config>			object_lib_t;
	typedef std::unordered_map<std::string, tile_config>	animations_t;
	typedef std::unordered_map<std::string, animations_t>	character_lib_t;
	typedef std::vector<object_ptr>							active_objects_t;
	typedef std::unordered_map<u32, character_ptr>			active_characters_t;
	typedef std::vector<position_t>							positions_t;

	void			load_tileset(const std::string& tileset);
	bool			load_xml(const std::string& path);
	void 			create_grid(u32 rows, u32 cols);
	void 			destroy_grid();
	bool			put_in_grid(map_object* obj, u32 row, u32 col, bool character = false);
	bool			check_obstacle(map_object* object, f32 x, f32 y) const;
	bool			can_move(map_object* obj, const glm::vec2& origin, const glm::vec2& dest) const;
	position_t		to_grid_coord(const glm::vec2& world_pos) const;

	u32					rows_;
	u32					cols_;
	tile**				grid_;

	tile_config			default_tile_;
	object_lib_t		object_lib_;
	character_lib_t		character_lib_;

	active_objects_t	active_objects_;
	active_characters_t	active_characters_;
	s32					curr_character_id_;
	s32					curr_character_z;
	positions_t			start_positions_;

	node_ptr			root_;
};

}
}

#endif
