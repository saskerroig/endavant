#include "map2d/map_character.h"
#include "Utils/callback.h"
#include "Renderer/Animators/CConstantLinearMovementAnimator.h"

namespace edv	{
namespace map2d	{

map_character::map_character(const tile_config& config) :
	map_object(config)
{

}

bool map_character::add_action(const std::string& id, const tile_config& config)
{
	auto it = actions_.find(id);

	if (it == actions_.end())
	{
		actions_.insert(it, actions_t::value_type(id, config));
		return true;
	}

	return false;
}

bool map_character::do_action(const std::string& id)
{
	auto it = actions_.find(id);

	if (it != actions_.end())
	{
		reset_sprite(it->second);
		return true;
	}

	return false;
}

void map_character::move(const path_t& path)
{
	position_.x = path.dest.x;
	position_.y = path.dest.y;

	current_path_.push_back(path);

	if (current_path_.size() == 1)
	{
		do_action("move");
		movement_finished(this, nullptr);
	}
}

void map_character::movement_finished(void* obj, void* caller)
{
	map_character* character	= static_cast<map_character*>(obj);
	auto& current_path			= character->current_path_;

	if (caller)
		current_path.pop_front();

	if (!current_path.empty())
	{
		const path_t& path = current_path.front();
		character->sprite_->AddAnimator(new CConstantLinearMovementAnimator(glm::vec3(path.origin, 1),
																			glm::vec3(path.dest, 1),
																			path.time,
																			util::callback(character, movement_finished)));
	}
	else
		character->do_action("default");
}

}
}
