#include "map2d/simple_ai.h"

namespace edv	{
namespace map2d	{

simple_ai::simple_ai(s32 id, f32 amount, f32 time) :
	id_(id),
	amount_(amount),
	time_(time),
	directions_{glm::vec2(amount_, 0),glm::vec2(0, amount_),glm::vec2(amount_*-1,0),glm::vec2(0, amount_*-1)},
	direction_(directions_[rand()%4])
{

}

void simple_ai::move(tile_map& map)
{
	if (!map.can_move_character(id_, direction_))
	{
		std::vector<glm::vec2> directions;
		directions.reserve(directions_.size());

		for (auto it = directions_.begin(); it != directions_.end(); it++)
		{
			if (*it != direction_ && map.can_move_character(id_, *it))
				directions.push_back(*it);
		}

		if (!directions.empty())
		{
			std::random_shuffle(directions.begin(), directions.end());
			direction_ = directions.front();
		}
	}

	map.move_character(id_, direction_, time_);
}

}
}
