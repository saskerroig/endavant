#ifndef EDV_MAP2D_TILE_H_
#define EDV_MAP2D_TILE_H_

#include "map2d/map_object.h"
#include <list>

namespace edv	{
namespace map2d	{

class tile
{
public:
	void add_object(map_object* object);
	void remove_object(map_object* object);
	bool empty() const;
	bool obstacle() const;

private:
	typedef std::list<map_object*>	objects_t;
	objects_t objects_;
};

}
}

#endif
