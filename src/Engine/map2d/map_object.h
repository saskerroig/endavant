#ifndef EDV_MAP2D_MAP_OBJECT_H_
#define EDV_MAP2D_MAP_OBJECT_H_

#include <memory>
#include "Renderer/Draw_Nodes/CSpriteAnimated.h"
#include "map2d/tile_config.h"

namespace edv	{
namespace map2d	{

class map_object
{
public:
	map_object(const tile_config& config);
	virtual ~map_object() { }

	bool				obstacle() const{ return obstacle_;		}
	u32					width()			{ return width_;		}
	u32					height()		{ return height_;		}
	CBaseNode*			node()			{ return sprite_.get();	}
	const glm::vec3&	position()		{ return position_;		}
	void				set_position(const glm::vec3& position);

protected:
	typedef std::unique_ptr<CSpriteAnimated>	sprite_ptr;

	void 		reset_sprite(const tile_config& config);

	bool				obstacle_;
	u32					width_;
	u32					height_;
	sprite_ptr			sprite_;
	glm::vec3			position_;
};

}
}

#endif
