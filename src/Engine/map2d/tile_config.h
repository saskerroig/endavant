#ifndef EDV_MAP2D_MAP_OBJECT_INFO_H_
#define EDV_MAP2D_MAP_OBJECT_INFO_H_

#include <string>
#include "Core/CBasicTypes.h"

namespace edv	{
namespace map2d	{

class tile_config
{
public:
	tile_config();
	tile_config(bool obstacle, bool character, u32 frames, f32 speed, const std::string& image, u32 tile_width, u32 tile_height);

	bool		obstacle_;
	bool		character_;
	u32			frames_;
	f32			speed_;
	std::string	image_;
	u32			tile_width_;
	u32			tile_height_;
};

}
}
#endif
