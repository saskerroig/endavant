#include "map2d/map_object.h"
#include "Utils/math.h"

namespace edv	{
namespace map2d	{

map_object::map_object(const tile_config& config) :
	obstacle_(config.obstacle_),
	width_(0),
	height_(0),
	sprite_(new CSpriteAnimated)
{
	reset_sprite(config);
}

void map_object::reset_sprite(const tile_config& config)
{
	sprite_->InitSpriteAnimated(config.image_, config.frames_, config.speed_);

	width_	= math::div_add_remainder(sprite_->GetSpriteWidth(), config.tile_width_);
	height_	= math::div_add_remainder(sprite_->GetSpriteHeight(), config.tile_height_);
}

void map_object::set_position(const glm::vec3& position)
{
	position_ = position;
	sprite_->SetPosition(position_);
}

}
}

