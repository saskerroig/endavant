/*
 * ITimerCallback.h
 *
 *  Created on: 28/07/2013
 *      Author: Sasker
 */

#ifndef ITIMERCALLBACK_H_
#define ITIMERCALLBACK_H_

typedef u32 				EV_TimerID;

class ITimerCallback
{
    public:
        virtual ~ITimerCallback() {}
        virtual void CallBack(EV_TimerID a_ID) = 0;
};

#endif /* ITIMERCALLBACK_H_ */
