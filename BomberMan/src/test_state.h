#ifndef TEST_STATE_H_
#define TEST_STATE_H_

#include "EVMain.h"
#include "map2d/tile_map.h"
#include "map2d/simple_ai.h"

class test_state : public CState, public ITimerCallback
{
public:
	test_state();

	virtual void Init();
	virtual void Cleanup();
	virtual void Pause();
	virtual void Resume();
	virtual void Update(f64 dt);

    virtual void CallBack(EV_TimerID a_ID);

	virtual ~test_state();

private:
	void	SetPFS();

	u32									m_SubState;

//	std::unique_ptr<CLayer>				m_layerbench;
//	std::vector<CSprite *>				m_graphicbench;
//
//	std::unique_ptr<CLayer>				m_layerbench2;
//	std::unique_ptr<CSpriteBatch>		m_graphicbench2;
//
//	std::unique_ptr<CSpriteBatch>			m_graphicmodel;
//	std::unique_ptr<CSprite>			m_graphicmodel2;
//	std::unique_ptr<CSpriteAnimated>	m_graphicbomberman;
//	std::unique_ptr<CSpriteAnimated>	m_graphicbombermanwalk;
	std::unique_ptr<CTextTTF>			m_text;
	std::unique_ptr<CTextTTF>			m_textfps;
	std::unique_ptr<CScene>				m_menuscene;

//	s32										m_playerid;
	edv::map2d::tile_map 					map;

	std::vector<std::unique_ptr<edv::map2d::simple_ai>> enemies;

//	CResourceLoader						*m_ResLoader;
};

#endif /* CMAINMENUSTATE_H_ */
