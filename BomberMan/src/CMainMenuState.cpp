#include "CMainMenuState.h"
#include "map2d/tile_map.h"

using namespace edv;

CMainMenuState::CMainMenuState()
{
	m_SubState = 0;

}

void CMainMenuState::Init()
{

	m_ResLoader = new CResourceLoader;

	m_graphicmodel =std::unique_ptr<CSpriteBatch>( new CSpriteBatch());
	m_graphicmodel->InitSprite("Textures/ansias2.png");
	m_graphicmodel->SetPosition(glm::vec3(90,90,0));
	m_graphicmodel->SetBatchSize(4);

	m_graphicmodel->SetBatchPositions( 0, glm::vec2(0,0) );
	m_graphicmodel->SetBatchPositions( 1, glm::vec2(1100,0) );
	m_graphicmodel->SetBatchPositions( 2, glm::vec2(0,540) );
	m_graphicmodel->SetBatchPositions( 3, glm::vec2(1100,540) );

	m_graphicmodel2 =std::unique_ptr<CSprite>( new CSprite());
	m_graphicmodel2->InitSprite("Textures/privado.png");
	m_graphicmodel2->SetPosition(glm::vec3(350,250,0));

	m_text =std::unique_ptr<CTextTTF>( new CTextTTF());
	m_text->InitTextTTF("Fonts/arial.ttf", 20);
	m_text->SetTextTTF("MIERDAA  AAAA", EV_Color{255,0,255,128});
	m_text->SetPosition(glm::vec3(10,680,10));

	m_textfps =std::unique_ptr<CTextTTF>( new CTextTTF());
	m_textfps->InitTextTTF("Fonts/courbd.ttf", 20);
	m_textfps->SetTextTTF("FPS: ", EV_Color{255,255,255,255}, TEXTUREALIGN_LEFTTOP );
	m_textfps->SetPosition(glm::vec3(10,720,2));

	m_graphicbomberman =std::unique_ptr<CSpriteAnimated>( new CSpriteAnimated());
	m_graphicbomberman->InitSpriteAnimated("Textures/downwait.png", 4, 0.25f);
	m_graphicbomberman->SetPosition(glm::vec3(250,450,1));
	m_graphicbomberman->Hide();

	m_graphicbombermanwalk =std::unique_ptr<CSpriteAnimated>( new CSpriteAnimated());
	m_graphicbombermanwalk->InitSpriteAnimated("Textures/down.xml", 6, 0.15f);
	m_graphicbombermanwalk->SetPosition(glm::vec3(250,450,1));
	m_graphicbombermanwalk->Hide();


	CreateBenchmark();

	m_menuscene	= std::unique_ptr<CScene>(new CScene());

	m_menuscene->AddChild(m_layerbench.get());
	m_menuscene->AddChild(m_layerbench2.get());

	m_menuscene->AddChild(m_graphicmodel.get());
	m_menuscene->AddChild(m_graphicmodel2.get());
	m_menuscene->AddChild(m_graphicbomberman.get());
	m_menuscene->AddChild(m_graphicbombermanwalk.get());
	m_menuscene->AddChild(m_text.get());
	m_menuscene->AddChild(m_textfps.get());

	CCoreEngine::Instance().GetRenderManager().PushScene(m_menuscene.get());

	m_text->SetTextTTF("LOADING...", EV_Color{255,128,0,128});

	m_SubState = 0;
	CCoreEngine::Instance().GetTimerManager().CreateTimer( 5.0f, true, this );
	//CCoreEngine::Instance().GetSoundManager().PlayEffect("Music/test.ogg");
}

void CMainMenuState::Cleanup()
{
	LOG( LOG_INFO, LOGSUB_GAME,"CLEANUP!");
	m_menuscene.reset();

	m_layerbench.reset();
	m_layerbench2.reset();

	for (auto &Element: m_graphicbench)
	{
		delete Element;
	}
	m_graphicbench.clear();

	m_graphicbench2.reset();

	m_graphicmodel.reset();
	m_graphicmodel2.reset();
	m_graphicbomberman.reset();
//	m_graphicbombermanwalk.reset(); //Comment to test auto free resources on resourcemanager shutdown
	m_text.reset();
	m_textfps.reset();

	delete m_ResLoader;
}

void CMainMenuState::Pause()
{
}

void CMainMenuState::Resume()
{
}

void CMainMenuState::Update(f64 dt)
{
	static f64 mydt = 0.0f;
	static u32 lastState = 0;
	u32 distancia;

	switch(m_SubState)
	{
		case 1:
			if (lastState != m_SubState)
			{
				m_graphicbomberman->Show();
			}
			break;
		case 2:
			if (lastState != m_SubState)
			{
				mydt = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
				m_graphicbomberman->Hide();
				m_graphicbombermanwalk->Show();
			}
			distancia = ((CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds() - mydt) * 40);
			m_graphicbombermanwalk->SetPosition( glm::vec2(250, (450 - distancia) ));
			m_graphicbomberman->SetPosition( glm::vec2(250, (450 - distancia) ));
			break;
		case 3:
			if (lastState != m_SubState)
			{
				m_graphicbomberman->Show();
				m_graphicbombermanwalk->Hide();
				m_layerbench->Show();
				m_layerbench->Enable();
			}
			break;
		case 4:
			if (lastState != m_SubState)
			{
				m_layerbench->Hide();
				m_layerbench->Disable();
				m_layerbench2->Show();
				m_layerbench2->Enable();
			}
			break;
		case 5:
			if (lastState != m_SubState)
			{
				m_graphicbench2->SetBatchMode( BATCHMODE_NODEPTH);
			}
			break;
		case 6:
			if (lastState != m_SubState)
			{
				m_layerbench2->Hide();
				m_layerbench2->Disable();
				CCoreEngine::Instance().GetResourceManager().LogResources();
			}
			break;
		case 7:
			if (lastState != m_SubState)
			{
				LOG( LOG_INFO, LOGSUB_GAME,"LOAD RESOURCES");
				bool rply = m_ResLoader->LoadResources( "resourcetest.xml", 50 );
				LOG( LOG_INFO, LOGSUB_GAME,"LOAD RESOURCES RESULT: %d", (s32)rply);
				CCoreEngine::Instance().GetResourceManager().LogResources();
			}
			break;
		case 8:
			if (lastState != m_SubState)
			{
				LOG( LOG_INFO, LOGSUB_GAME,"RELEASE RESOURCES");
				bool rply = m_ResLoader->ReleaseResources( "resourcetest.xml" );
				LOG( LOG_INFO, LOGSUB_GAME,"RELEASE RESOURCES RESULT: %d", (s32)rply);
				CCoreEngine::Instance().GetResourceManager().LogResources();
			}
			break;
		case 10:
			if (lastState != m_SubState)
			{
				LOG( LOG_INFO, LOGSUB_GAME,"LOAD RESOURCES ASYNC");
				m_ResLoader->LoadResourcesASync( "resourcetest.xml" );
			}
			break;
		case 12:
			if (lastState != m_SubState)
			{
				if (m_ResLoader->IsLoaded())
				{
					LOG( LOG_INFO, LOGSUB_GAME,"LOAD RESOURCES ASYNC");
					CCoreEngine::Instance().GetResourceManager().LogResources();
				}
			}
			break;
		case 13:
			if (lastState != m_SubState)
			{
				LOG( LOG_INFO, LOGSUB_GAME,"RELEASE RESOURCES");
				bool rply = m_ResLoader->ReleaseResources( "resourcetest.xml" );
				LOG( LOG_INFO, LOGSUB_GAME,"RELEASE RESOURCES RESULT: %d", (s32)rply);
				CCoreEngine::Instance().GetResourceManager().LogResources();
			}
			break;
		case 14:
			if (lastState != m_SubState)
			{
//				LOG( LOG_INFO, LOGSUB_GAME,"MAP INIT");
//
//				map2d::tile_map map("Config/tileset1.xml");
//				map.load_xml("Maps/level1.xml");
//				m_menuscene->AddChild(map.root());
//
//				LOG( LOG_INFO, LOGSUB_GAME,"MAP END");
			}
			break;

		default:
			break;
	}
	lastState = m_SubState;

	SetPFS();
}

CMainMenuState::~CMainMenuState()
{

}

void CMainMenuState::CreateBenchmark()
{
	CSprite *SpriteBench;

	m_layerbench =std::unique_ptr<CLayer>( new CLayer());
	m_layerbench->SetPosition(glm::vec3(0, 0, -200) );
	m_layerbench->Hide();
	m_layerbench->Disable();

	for ( u32 count = 0; count < 5000; count++)
	{
		SpriteBench = new CSprite();
		SpriteBench->InitSprite("Textures/privado.png");
		SpriteBench->SetPosition(glm::vec3(100 + rand()%1000,100 + rand()%500 , 0 + ((f32)count/100.0f) ));

		m_layerbench->AddChild(SpriteBench);
		m_graphicbench.push_back(SpriteBench);
	}

	m_layerbench2 =std::unique_ptr<CLayer>( new CLayer());
	m_layerbench2->SetPosition(glm::vec3(0, 0, -200) );
	m_layerbench2->Hide();
	m_layerbench2->Disable();

	m_graphicbench2 =std::unique_ptr<CSpriteBatch>( new CSpriteBatch());
	m_graphicbench2->InitSprite("Textures/privado.png");
	m_graphicbench2->SetPosition(glm::vec3(0,0,0));
	m_graphicbench2->SetBatchSize(5000);

	for ( u32 count = 0; count < 5000; count++)
	{
		m_graphicbench2->SetBatchPositions( count, glm::vec2( 100 + rand()%1000,100 + rand()%500 ) );
	}

	m_layerbench2->AddChild(m_graphicbench2.get());
}

void CMainMenuState::CallBack(EV_TimerID a_ID)
{
	m_SubState++;
	LOG( LOG_INFO, LOGSUB_GAME,"CallBack Timer called! %u", m_SubState);

	std::stringstream ss;
	ss << m_SubState;
	std::string test("TEST " + ss.str());
	m_text->SetTextTTF(test, EV_Color{255,128,0,128});
}

void CMainMenuState::SetPFS()
{
	static f64 lastfps = 0.0f;
	f64 actfps = CCoreEngine::Instance().GetTimerManager().GetFPS();

	if (lastfps != actfps )
	{
		char buf[80];
		lastfps = actfps;
		sprintf( buf, "FPS: %.02f", lastfps);
		m_textfps->SetTextTTF( buf, EV_Color{255,255,255,255}, TEXTUREALIGN_LEFTTOP );
	}
}
