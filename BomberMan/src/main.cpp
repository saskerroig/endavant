#include "EVMain.h"
#include "test_state.h"

int main(int argc, char *argv[])
{
	CCoreEngine &l_core= CCoreEngine::Instance();
	l_core.StartUp();

//	CMainMenuState l_mainmenustate;
	test_state test;
	l_core.GetStateManager().ChangeState(&test);

	l_core.Run();

	l_core.ShutDown();

	return 0;
}



