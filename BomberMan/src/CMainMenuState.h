/*
 * CMainMenuState.h
 *
 *  Created on: 08/07/2013
 *      Author: Dani
 */

#ifndef CMAINMENUSTATE_H_
#define CMAINMENUSTATE_H_

#include "EVMain.h"

class CMainMenuState: public CState, public ITimerCallback
{
public:
	CMainMenuState();

	virtual void Init();
	virtual void Cleanup();
	virtual void Pause();
	virtual void Resume();
	virtual void Update(f64 dt);

    virtual void CallBack(EV_TimerID a_ID);

	virtual ~CMainMenuState();

private:
	void	CreateBenchmark();
	void	SetPFS();

	u32									m_SubState;

	std::unique_ptr<CLayer>				m_layerbench;
	std::vector<CSprite *>				m_graphicbench;

	std::unique_ptr<CLayer>				m_layerbench2;
	std::unique_ptr<CSpriteBatch>		m_graphicbench2;

	std::unique_ptr<CSpriteBatch>			m_graphicmodel;
	std::unique_ptr<CSprite>			m_graphicmodel2;
	std::unique_ptr<CSpriteAnimated>	m_graphicbomberman;
	std::unique_ptr<CSpriteAnimated>	m_graphicbombermanwalk;
	std::unique_ptr<CTextTTF>			m_text;
	std::unique_ptr<CTextTTF>			m_textfps;
	std::unique_ptr<CScene>				m_menuscene;

	CResourceLoader						*m_ResLoader;
};

#endif /* CMAINMENUSTATE_H_ */
