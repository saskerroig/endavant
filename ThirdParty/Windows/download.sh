#!/bin/bash

hg clone http://hg.libsdl.org/SDL
wget http://zlib.net/zlib-1.2.8.tar.gz
wget http://sourceforge.net/projects/freetype/files/freetype2/2.4.10/freetype-2.4.10.tar.gz
wget http://sourceforge.net/projects/libpng/files/libpng15/1.5.16/libpng-1.5.16.tar.xz/download
hg clone http://hg.libsdl.org/SDL_image/
hg clone http://hg.libsdl.org/SDL_ttf/
wget http://downloads.xiph.org/releases/ogg/libogg-1.3.1.tar.gz
wget http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.3.tar.gz
hg clone http://hg.libsdl.org/SDL_mixer/