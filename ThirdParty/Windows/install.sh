#!/bin/bash



cd SDL
mkdir build
cd build
../configure
make
make install
cp -R -f /usr/local/* /c/MinGW/$1
cd

####

tar xvfz zlib-1.2.8.tar.gz
cd zlib-1.2.8
make -f win32/Makefile.gcc BINARY_PATH=/usr/local/bin INCLUDE_PATH=/usr/local/include LIBRARY_PATH=/usr/local/lib install
cd ..
cp -R -f /usr/local/* /c/MinGW/$1

cd
###


tar zxvf freetype-2.4.10.tar.gz  
cd freetype-2.4.10  
./configure --enable-static  
make install  
cp -R -f /usr/local/* /c/MinGW/$1
cd

####

tar xvfJ libpng-1.5.16.tar.xz
cd libpng-1.5.16
mv INSTALL INSTALL.txt
./configure
make install
cd ..
cp -R -f /usr/local/* /c/MinGW/$1
cd

###


cd SDL_image
mkdir build
cd build
../configure
make
make install
cp -R -f /usr/local/* /c/MinGW/$1
cd
###


cd SDL_ttf
mkdir build
cd build
../configure
make
make install
cp -R -f /usr/local/* /c/MinGW/$1
cd
###


tar zxvf libogg-1.3.1.tar.gz  
cd libogg-1.3.1  
./configure --enable-static  
make install  
cp -R -f /usr/local/* /c/MinGW/$1
cd
####


tar zxvf libvorbis-1.3.3.tar.gz
cd libvorbis-1.3.3  
./configure --enable-static  
make install  
cp -R -f /usr/local/* /c/MinGW/$1
cd
###


cd SDL_mixer
mkdir build
cd build
../configure
make
make install
cp -R -f /usr/local/* /c/MinGW/$1
cd
####



