#!/bin/bash


echo "$1"
echo "Intalling SDL..."
cd SDL
mkdir build
cd build
../configure
make
make install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ../../

echo "Intalling SDL... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
####

echo "Intalling zlib..."
tar xvfz zlib-1.2.8.tar.gz
cd zlib-1.2.8
make -f win32/Makefile.gcc BINARY_PATH=/usr/local/bin INCLUDE_PATH=/usr/local/include LIBRARY_PATH=/usr/local/lib install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ..

echo "Intalling zlib... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
###


echo "Intalling freetype..."
tar zxvf freetype-2.4.10.tar.gz  
cd freetype-2.4.10  
./configure --enable-static  
make install  
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ..

echo "Intalling freetype... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
####

echo "Intalling libpng..."
tar xvfJ libpng-1.5.16.tar.xz
cd libpng-1.5.16
mv INSTALL INSTALL.txt
./configure
make install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ..

echo "Intalling libpng... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
###


echo "Intalling SDL_image..."
cd SDL_image
mkdir build
cd build
../configure
make
make install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ../../

echo "Intalling SDL_image... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
###

echo "Intalling SDL_ttf..."
cd SDL_ttf
mkdir build
cd build
../configure
make
make install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ../../

echo "Intalling SDL_ttf... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
###


echo "Intalling libogg..."
tar zxvf libogg-1.3.1.tar.gz  
cd libogg-1.3.1  
./configure --enable-static  
make install  
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ..

echo "Intalling libogg... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
####

echo "Intalling libvorbis..."
tar zxvf libvorbis-1.3.3.tar.gz
cd libvorbis-1.3.3  
./configure --enable-static  
make install  
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ..

echo "Intalling libvorbis... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
###


echo "Intalling SDL_mixer..."
cd SDL_mixer
mkdir build
cd build
../configure
make
make install
cp -R -f -v /usr/local/* /c/MinGW/$1/
cp -R -f -v /usr/local/* /c/MinGW/
cd ../../

echo "Intalling SDL_mixer... END"
echo "####################"
echo "####################"
echo "####################"
echo "####################"
####



